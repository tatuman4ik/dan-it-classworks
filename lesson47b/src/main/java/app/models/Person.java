package app.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "person")
public class Person {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String name;

  @OneToOne
  @JoinColumn(name = "x_id", referencedColumnName = "id")
  //               t: person                      t: extra
  private Extra extra;

}
