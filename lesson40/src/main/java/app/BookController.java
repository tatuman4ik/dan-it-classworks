package app;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class BookController {

  private final BookService service;

  @GetMapping("books")
  // 1.servlet
  // 2.GET
  // 3.List<Book> -> JSON
  // 4.JSON -> ResponseBody
  public List<Book> allBooks() {
    return service.allBooks();
  }

  public ResponseEntity<List<Book>> allBooks1() {
    List<Book> books = service.allBooks();

    return books.isEmpty() ?
      ResponseEntity.status(204).build() :
      ResponseEntity.status(200).body(books);
  }

  private String serialize(List<Book> books) {
    throw new IllegalArgumentException();
  }

  public ResponseEntity<String> allBooks2() {
    List<Book> books = service.allBooks();

    return books.isEmpty() ?
      ResponseEntity.status(204).build() :
      ResponseEntity.status(200).body(serialize(books));
  }

  // http://localhost:8080/book?id=1
  @GetMapping("boook")
  public Book bookById1(@RequestParam("id") Integer n) {
    return service.makeBook(n);
  }

  // http://localhost:8080/book?id=1
  @GetMapping("book")
  public Book bookById2(@RequestParam("id") Optional<Integer> n) {
    return service.allBooks().get(n.orElse(3) - 1);
  }

  // http://localhost:8080/book/1
  @GetMapping("book/{id}")
  public Book bookById3(@PathVariable("id") Integer n) {
    return service.allBooks().get(n - 1);
  }

}
