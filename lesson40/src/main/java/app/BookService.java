package app;

import java.util.List;

public interface BookService {

  List<Book> allBooks();
  Book makeBook(int delta);

}
