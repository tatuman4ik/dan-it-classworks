package app;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceMocked implements BookService {

  private final List<Book> books = List.of(
      new Book("Java", "Jim", 2007),       // 1
      new Book("Closure", "Nate", 2012),   // 2
      new Book("Scala32", "Jack", 2022)      // 3
  );

  @Override
  public List<Book> allBooks() {
    return books;
  }

  @Override
  public Book makeBook(int delta) {
    return new Book("Javascript", "somebody", 2000 + delta);
  }

}
