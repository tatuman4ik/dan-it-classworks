package app.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Extra {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String extraDetails;

  @OneToOne
  @MapsId
  private Person person;
}
