package app.controller;

import app.models.ApiError;
import app.models.Person;
import app.models.PersonDetails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("person")
@PropertySource("classpath:api-secret.properties")
public class PersonController {

  @Value("${api.code}")
  String code;

  @GetMapping("open")
  public Person handleGet(@RequestParam("id") Integer id) {
    return new Person(id, "Jim");
  }

  @GetMapping
  public ResponseEntity<?> handleGet(@RequestParam("id") Integer id, @RequestHeader(HttpHeaders.AUTHORIZATION) String auth) {
    if (!auth.equals(code))
      return ResponseEntity
          .status(HttpStatus.FORBIDDEN)
          .body(new ApiError("Authorization header is required"));

    var p = new Person(id, "Jim");
    return ResponseEntity.ok(p);
  }

  @PostMapping
  public ResponseEntity<?> handlePost(@RequestBody PersonDetails body, @RequestHeader(HttpHeaders.AUTHORIZATION) String auth) {
    if (!auth.equals(code))
      return ResponseEntity
          .status(HttpStatus.FORBIDDEN)
          .body(new ApiError("Authorization header is required"));

    if (body.id() == 13)
      return ResponseEntity
          .status(HttpStatus.CONFLICT)
          .body(new ApiError("id provided already exists"));

    var p = new Person(body.id(), body.name());
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(p);
  }

}
