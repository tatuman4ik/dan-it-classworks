package app.models;

public record ApiError(String message) {
}
