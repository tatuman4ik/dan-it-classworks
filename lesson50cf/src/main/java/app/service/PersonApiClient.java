package app.service;

import app.models.Person;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "personApiClient", url = "http://localhost:8080")
public interface PersonApiClient {

  @RequestMapping(method = RequestMethod.GET, value = "/person/open", produces = "application/json")
  Person get(@RequestParam("id") Integer id);

}
