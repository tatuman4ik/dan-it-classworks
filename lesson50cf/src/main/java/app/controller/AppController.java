package app.controller;

import app.models.Person;
import app.service.PersonApiClient;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AppController {

  private final PersonApiClient personApiClient;

  @GetMapping("p")
  public Person handle() {
    return personApiClient.get(1111);
  }

}
