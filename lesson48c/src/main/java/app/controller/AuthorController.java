package app.controller;

import app.dto.AuthorRs;
import app.dto.BookRs;
import app.models.Author;
import app.repo.AuthorRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("a")
@RequiredArgsConstructor
public class AuthorController {

  private final AuthorRepo authorRepo;

  // TODO: FetchType OR FetchMode
  //  FetchType = LAZY
  @GetMapping
  public List<AuthorRs> handle() {
    Optional<Author> byId1 = authorRepo.findById(1); // EAGER -> + SELECT book
    Optional<Author> byId2 = authorRepo.findById(1); // LAZY -> book = null
    //                                               //   getBook -> SELECT

    List<Author> authorEntities = authorRepo.findAll();
    List<AuthorRs> data = authorEntities.stream().map(a ->
        new AuthorRs(a.getId(), a.getName(),
            a.getBooks()
                .stream()
                .map(b -> new BookRs(b.getId(), b.getName()))
                .toList()
        )).toList();
    return data;
  }

}
