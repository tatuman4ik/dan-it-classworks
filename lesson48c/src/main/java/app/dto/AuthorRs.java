package app.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class AuthorRs {
  Integer id;
  String name;
  List<BookRs> books;
}
