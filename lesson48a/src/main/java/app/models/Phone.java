package app.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Phone {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ph_id")
  Integer id;

  @Column(name = "ph_number")
  String number;

  @ManyToOne
  @JoinColumn(name = "p_id")
  Person person;

}
