package app.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Person {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "p_id")
  private Integer id;

  @Column(name = "p_name")
  private String name;

  @OneToMany(mappedBy = "person")
  private Set<Phone> phones;

}
