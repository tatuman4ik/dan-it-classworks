import lesson08.Calc;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalcTest {

    @Test
    public void test1() {
        assertEquals(3, 1 + 2);
    }

    @Test
    public void test2() {
        assertEquals("HELLO", "Hello".toUpperCase());
    }

    @Test
    public void test3() {
        assertEquals(2, Calc.add(1, 1));
    }

    @Test
    public void test4() {
        assertEquals(15, Calc.mul(5, 3));
    }

}
