package lesson11;

import java.util.function.BiFunction;
import java.util.function.Function;

public class AbstractVsInterface {

    static abstract class Animal {
        int x;
    }
    static abstract class Bird {}

    static class Dinosaur1 extends Animal {}

    interface Move {
//        int x;
        void move();
    }
    interface Swim {
        void dive();
    }
    interface Jump {}
    interface Fly {}

    static abstract class Dinosaur2 implements Move, Swim, Jump, Fly {}
    static class Dinosaur3 implements Move, Swim, Jump, Fly {
        @Override
        public void move() {

        }

        @Override
        public void dive() {

        }
    }



    public static void main(String[] args) {
        new Dinosaur2() {
            @Override
            public void dive() {

            }

            @Override
            public void move() {
                System.out.println("mmm");
            }
        };

        Move m1 = () -> System.out.println("moving");

        //            a        b      a + b
        BiFunction<Integer, Integer, Integer> add6 = new BiFunction<>() {
            @Override
            public Integer apply(Integer a, Integer b) {
                return a + b;
            }
        };
        BiFunction<Integer, Integer, Integer> add5 = (Integer a, Integer b) -> {
            return a + b;
        };
        BiFunction<Integer, Integer, Integer> add4 = (Integer a, Integer b) -> a + b;
        BiFunction<Integer, Integer, Integer> add3 = (a, b) -> a + b;
        BiFunction<Integer, Integer, Integer> add2 = (a, b) -> Integer.sum(a, b);
        BiFunction<Integer, Integer, Integer> add1 = Integer::sum;

    }

}
