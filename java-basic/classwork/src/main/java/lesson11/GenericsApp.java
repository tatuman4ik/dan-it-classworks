package lesson11;

import lesson06.Pizza;
import lesson10.Inheritance.*;

import java.util.ArrayList;
import java.util.HashSet;

public class GenericsApp {

    static class Box1 {
        final Integer x;

        public Box1(Integer x) {
            this.x = x;
        }

        public Integer getX() {
            return x;
        }
    }

    static class Box2<A> {
        final A x;

        public Box2(A x) {
            this.x = x;
        }

        public A getX() {
            return x;
        }
    }

    public static void main(String[] args) {
        new Box2<Integer>(1);
        new HashSet<>();
        ArrayList<Integer> integers = new ArrayList<Integer>();
        ArrayList<String> strings = new ArrayList<String>();
        ArrayList<Pizza> pizzas = new ArrayList<Pizza>();
        ArrayList<Cat> cats = new ArrayList<>();
        ArrayList<Dog> dogs = new ArrayList<>();
        ArrayList<Animal> animals = new ArrayList<Animal>();
        animals.add(new Cat());
        animals.add(new Dog());

        for (Animal a: animals) {
            a.sound();
        }


    }

}
