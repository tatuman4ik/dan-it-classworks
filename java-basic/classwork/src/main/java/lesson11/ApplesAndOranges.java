package lesson11;

import java.util.List;

public class ApplesAndOranges {

    static boolean inRange(int pos, int l, int r) {
        return pos >= l && pos <= r;
    }

    static int count0(List<Integer> distances, int center, int l, int r) {
        int cnt = 0;
        for (int distance: distances) {
            cnt += inRange(center + distance, l, r) ? 1 : 0;
        }
        return cnt;
    }

    static long count(List<Integer> distances, int center, int l, int r) {
        return distances.stream()
                .map(x -> center + x)
                .filter(pos -> inRange(pos, l, r))
                .count();
    }

    public static void countApplesAndOranges(
            int s, int t, int a, int b,
            List<Integer> apples,
            List<Integer> oranges) {

        long ac = count(apples, a, s, t);
        long oc = count(oranges, b, s, t);

        System.out.printf("%d\n%d\n", ac, oc);
    }

}
