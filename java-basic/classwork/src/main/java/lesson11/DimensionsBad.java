package lesson11;

public class DimensionsBad {

    static void fill(String[][] data) {
        data[0][0] = "Monday";
        data[0][1] = "free";
        data[1][0] = "Tuesday";
        data[1][1] = "busy";
    }

    public static void main(String[] args) {

        String[][] data = new String[10][2];
        fill(data);
        String status = data[0][0];
        String day = data[0][1];
        System.out.printf("status %s\n", status);
        System.out.printf("day %s\n", day);

    }

}
