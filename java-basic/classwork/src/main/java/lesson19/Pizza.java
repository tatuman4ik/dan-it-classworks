package lesson19;

import java.io.Serializable;
import java.util.Optional;

public class Pizza implements Serializable {
  final String name;
  final Optional<Integer> size = Optional.empty();

  public Pizza(String name) {
    this.name = name;
  }
}
