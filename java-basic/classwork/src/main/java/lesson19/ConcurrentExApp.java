package lesson19;

import java.util.Arrays;

public class ConcurrentExApp {

  public static void main(String[] args) {
    int[] x1 = {1, 2, 3};

    int[] x2 = Arrays.stream(x1)
        .map(x -> x*10)
//        .filter(a -> a > 1)
        .toArray();

    System.out.println(Arrays.toString(x2));
  }
}
