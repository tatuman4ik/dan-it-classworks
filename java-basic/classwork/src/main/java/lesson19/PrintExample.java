package lesson19;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class PrintExample {

  interface Console {
    void print(String s);
  }

  static class ConsoleReal implements Console {

    @Override
    public void print(String s) {
      System.out.println(s);
    }

  }

  static class ConsoleTest implements Console {

    private List<String> storage = new ArrayList<>();

    @Override
    public void print(String s) {
      storage.add(s);
    }

    public List<String> getOutput() {
      return storage;
    }
  }

  static void print(Console console) {
    console.print("Hello");
  }

  public static void main0(String[] args) {
    Console console = new ConsoleReal();
    print(console);
  }

  // test
  public static void main(String[] args) {
//    Collections.sort();
    ConsoleTest console = new ConsoleTest();
    print(console);
    assert (console.getOutput().get(0).equals("Hello"));

    record Person(int id, String name) {
    }

    Stream.of(1, 2, 3)
        .flatMap(x ->
            Stream.of("Jim", "Bim")
                .map(y ->
                    new Person(x, y)
                )
        )
        .forEach(System.out::println);


  }


}
