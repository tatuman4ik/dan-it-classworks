package lesson19;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class PizzaApp {

  public static void main(String[] args) throws IOException {
    FileOutputStream fos = new FileOutputStream("pz.bin");
    ObjectOutputStream oos = new ObjectOutputStream(fos);
    oos.writeObject(new Pizza("4s"));
    oos.close();
  }
}
