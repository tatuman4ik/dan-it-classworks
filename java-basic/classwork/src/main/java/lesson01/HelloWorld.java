package lesson01;

public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        String s1 = "Jim";
        System.out.println(s1.length());          // 3
        System.out.println(s1.getBytes().length); // 3
        String s2 = "Алекс";
        System.out.println(s2.length());          // 5
        System.out.println(s2.getBytes().length); // 10
    }

}
