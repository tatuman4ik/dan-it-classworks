package lesson15a.text;

import lesson15a.Student;

import java.io.*;

public class AppRead {

  static Student read(BufferedReader r) throws IOException {
    String line1 = r.readLine();
    String line2 = r.readLine();
    return new Student(line1, Integer.parseInt(line2));
  }

  public static void main(String[] args) throws IOException {

    BufferedReader r = new BufferedReader(new FileReader("students.txt"));

    Student s1 = read(r);
    Student s2 = read(r);

    r.close();
  }

}
