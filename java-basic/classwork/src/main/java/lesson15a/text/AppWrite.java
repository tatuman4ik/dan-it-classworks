package lesson15a.text;

import lesson15a.Student;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class AppWrite {

  static void storeText(Student s, BufferedWriter w) throws IOException {
    w.write(s.name);
    w.write("\n");
    w.write(Integer.toString(s.id));
    w.write("\n");
  }

  public static void main(String[] args) throws IOException {
    Student s1 = new Student("Jim123", 13);
    Student s2 = new Student("Jack", 20);

    BufferedWriter w = new BufferedWriter(new FileWriter("students.txt"));


    storeText(s1, w);
    storeText(s2, w);


    w.close();
  }

}
