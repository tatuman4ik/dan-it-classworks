package lesson15a.bin2;

import lesson15a.Student;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class AppWrite {

  public static void main(String[] args) throws IOException {
    Student s1 = new Student("Jim123", 13);
    Student s2 = new Student("Jack", 20);

    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("students2.bin"));

    ArrayList<Student> as = new ArrayList<>();
    as.add(s1);
    as.add(s2);

    oos.writeObject(as);

    oos.close();
  }

}
