package lesson15a.bin2;

import lesson15a.Student;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class AppRead {

  public static void main(String[] args) throws IOException, ClassNotFoundException {

    ObjectInputStream ois = new ObjectInputStream(new FileInputStream("students2.bin"));

    ArrayList<Student> readed = (ArrayList<Student>) ois.readObject();

    for (Student s: readed) {
      System.out.println(s.id);
      System.out.println(s.name);
    }

    ois.close();
  }

}
