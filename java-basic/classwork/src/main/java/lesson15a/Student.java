package lesson15a;

import java.io.Serializable;

public class Student implements Serializable {

  public String name;
  public Integer id;

  public Student(String name, Integer id) {
    this.name = name;
    this.id = id;
  }

}
