package lesson15a.bin1;

import lesson15a.Student;

import java.io.*;

public class AppWrite {

  static void storeBin(Student s, ObjectOutputStream oos) throws IOException {
    oos.writeObject(s);
  }

  public static void main(String[] args) throws IOException {
    Student s1 = new Student("Jim123", 13);
    Student s2 = new Student("Jack", 20);

    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("students.bin"));

    storeBin(s1, oos);
    storeBin(s2, oos);

    oos.close();
  }

}
