package lesson15a.bin1;

import lesson15a.Student;

import java.io.*;

public class AppRead {

  static Student read(ObjectInputStream ois) throws IOException, ClassNotFoundException {
    Student readed = (Student) ois.readObject();
    return readed;
  }

  public static void main(String[] args) throws IOException, ClassNotFoundException {

    ObjectInputStream ois = new ObjectInputStream(new FileInputStream("students.bin"));

    Student s1 = read(ois);
    Student s2 = read(ois);
    Student s3 = read(ois);

    System.out.println(s1.id);
    System.out.println(s1.name);
    System.out.println(s2.id);
    System.out.println(s2.name);

    ois.close();
  }

}
