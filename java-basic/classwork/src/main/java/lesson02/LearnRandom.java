package lesson02;

public class LearnRandom {

    // random int from min to max, including
    public static int random(int min, int max) {
        int range = max - min + 1; // 0 .... max - min + 1
        double rand = Math.random() * range;
        int result = (int) (rand + min);
        return result;
    }

    public static void main(String[] args) {
        double r = Math.random();  //  [0..1)
        double r1 = r * 0.5;        //  0..0.5
        double r2 = r * 10;        //   0..10
        double r3 = r * 20 - 10;   // -10..10
        double r4 = r * 180 - 30;  // -30..150
        double r5 = r * 180 - 150; // -150..30
//        int i  = (int) Math.random(); // 0
//        int i2 = (int) r * 10;        // 0
//        int i3 = (int) r * 20 - 10;   // -10
//        int i4 = (int) r * 180 - 30;  // -30
//        int i5 = (int) r * 180 - 150; // -150
        int i2 = (int) (r * 10);        //   0..10
        int i3 = (int) (r * 20) - 10;   //  -10..10
        int i4 = (int) (r * 180) - 30;  //  -30..150
        int i5 = (int) (r * 180) - 150; // -150..30
        int i6 = (int) (r * 200) + 100; //  100..300
    }

}
