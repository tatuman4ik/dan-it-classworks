package lesson02;

import java.io.PrintStream;

public class Output {

    public static void main(String[] args) {
        PrintStream out = System.out; // normal output
        PrintStream err = System.err; // errors only
        out.println("Hello");
        err.println("Something went wrong");
    }

}
