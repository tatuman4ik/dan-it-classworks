package lesson02;

public class TypeCast {

    public static void main(String[] args) {
        var a = 10; // int
        int x = 32770;
        short y = (short) x; // -32766
        System.out.println(y);
        long z = x; // 32770
        System.out.println(z);
    }

}
