package lesson02;

import java.io.InputStream;
import java.util.Scanner;

public class Input2 {

    public static void main(String[] args) {
        InputStream in = System.in;
        var scanner = new Scanner(in);

        System.out.print("Enter the number: ");

        int i = scanner.nextInt();

        System.out.println("You entered " + i);
    }

}
