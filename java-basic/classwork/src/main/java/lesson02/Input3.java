package lesson02;

import java.io.InputStream;
import java.util.Scanner;

public class Input3 {

    public void doSomething(String input) {
        // ...
    }

    public static void main(String[] args) {
        InputStream in = System.in;
        var scanner = new Scanner(in);

        System.out.print("Enter the number: ");
        String line = scanner.nextLine();
//        scanner.hasNextInt() // ok for homework

        try {
            int i = Integer.parseInt(line);
            System.out.printf("Number %d was successfully parsed", i);
        } catch (NumberFormatException ex) {
            System.out.printf("String %s wasn't successfully parsed\n", line);
            System.out.printf("Exception was: %s\n", ex.getMessage());
        }
    }

}
