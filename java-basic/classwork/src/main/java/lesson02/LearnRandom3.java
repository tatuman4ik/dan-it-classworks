package lesson02;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class LearnRandom3 {

    public static String arrayToString(int[] xs) {
        return Arrays.toString(xs);
    }

    public static void main(String[] args) {
        Random random = new Random();
        IntStream intStream1 = random.ints(10, 100);                // unlimited
        IntStream intStream2 = intStream1.limit(20); // limited to 20
        int[] ints = intStream2.toArray();
        String line = arrayToString(ints);
        System.out.println(line);
    }

}
