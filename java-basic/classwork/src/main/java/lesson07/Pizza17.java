package lesson07;

import java.util.Arrays;

public record Pizza17(String name, Integer size, String[] extra) {

    public Pizza17(String name, Integer size) {
        // String , Integer , String[]
        this(name, size, new String[]{});
    }

    public Pizza17 withExtra(String oneMoreExtra) {
        String[] extra2 = Arrays.copyOf(extra, extra.length + 1);
        extra2[extra2.length-1] = oneMoreExtra;
        return new Pizza17(name, size, extra2);
    }

    public static Pizza17 imdrunk() {
        return new Pizza17("Margarita", 60, new String[]{"pepper"});
    }



    public static void main(String[] args) {
        Pizza17 p1 = new Pizza17("Margarita", 30);
        System.out.println(p1);
        Pizza17 p2 = p1.withExtra("pepper");
    }

}
