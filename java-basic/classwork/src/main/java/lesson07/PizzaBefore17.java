package lesson07;

public class PizzaBefore17 {

    public final String name;
    public final Integer size;

    public PizzaBefore17(String name, Integer size) {
        this.name = name;
        this.size = size;
    }

}
