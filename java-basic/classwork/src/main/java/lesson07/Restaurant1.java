package lesson07;

import libs.EX;

public class Restaurant1 {

    static boolean myEquals(Pizza p1, Pizza2 p2) {
        throw EX.NI;
    }

    public static String makeName() {
        return "Marga" + "RITA".toLowerCase();
    }

    public static void main(String[] args) {
        Pizza p1 = new Pizza(makeName(), 60);
        Pizza p2 = new Pizza(makeName(), 60);
        Pizza p3 = new Pizza("Margarita", 60);
        Pizza p4 = new Pizza("Margarita", 60);
        Pizza2 p5 = new Pizza2("Margarita", 60);
        Pizza p11 = p1;

        System.out.println(p1 == p11);
        System.out.println(p1 == p2);
//        System.out.println(p1 == p5);
//        boolean b = myEquals(p1, p5);
    }

}
