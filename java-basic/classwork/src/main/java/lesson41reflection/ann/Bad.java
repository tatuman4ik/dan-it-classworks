package lesson41reflection.ann;

public @interface Bad {
  int value();
  String author() default "Alex";
}
