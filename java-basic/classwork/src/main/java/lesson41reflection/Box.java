package lesson41reflection;

import lesson41reflection.ann.Bad;
import lesson41reflection.ann.Good;

@Good
public class Box {

  @Good
  private final int x;

  @Bad(value = 10)
  public Box(@Good int x) {
    this.x = x;
  }

  @Bad(value = 50, author = "Jim")
  public void print() {
    System.out.printf("I'm Box, inside: %d\n", x);
  }

  @Bad(value = 70, author = "Sergio")
  public void print2(String prefix) {
    System.out.printf("%s I'm Box, inside: %d\n", prefix, x);
  }

}
