package lesson41reflection;

import lesson41reflection.ann.Ripe;
import lesson41reflection.ann.Run;
import lesson41reflection.fruits.Fruit;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

public class Playground2 {

  interface CallableNoEx<A> extends Callable<A> {
    default A callNoEx() {
      try {
        return call();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }

  public static List<CallableNoEx<String>> buildApp() {
    ArrayList<CallableNoEx<String>> data = new ArrayList<>();

    new Reflections(new ConfigurationBuilder()
        .setScanners(new SubTypesScanner(false), new ResourcesScanner())
        .addUrls(ClasspathHelper.forJavaClassPath())
        .filterInputsBy(new FilterBuilder().include(
            FilterBuilder.prefix("lesson41reflection/fruits")
        ))
    )
        .getSubTypesOf(Fruit.class)
        .stream()
        .filter(clazz -> clazz.isAnnotationPresent(Ripe.class))
        .filter(clazz -> {
          Ripe annotation = clazz.getDeclaredAnnotation(Ripe.class);
          return annotation.value() > 50;
        })
        .filter(clazz -> Arrays.stream(clazz.getMethods())
            .anyMatch(m -> m.isAnnotationPresent(Run.class))
        )
        .forEach((Class<?> clazz) -> {
          try {
            Constructor<?> constructor = clazz.getConstructor();
            Object instance = constructor.newInstance();
            Method method = Arrays.stream(clazz.getMethods())
                .filter(m -> m.isAnnotationPresent(Run.class))
                .findAny()
                .orElseThrow(() -> new RuntimeException("impossible by design"));
            data.add(() -> (String) method.invoke(instance));
//            String outcome = (String) method.invoke(instance);
//            System.out.println(outcome);
          } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
                   IllegalAccessException e) {
            throw new RuntimeException(e);
          }
        });

    return data;
  }

  public static void main(String[] args) {
    buildApp()
        .forEach(x -> System.out.println(x.callNoEx()));
  }

}
