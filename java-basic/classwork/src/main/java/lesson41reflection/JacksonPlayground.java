package lesson41reflection;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
class Person {
  Integer id;
  String name;
}

public class JacksonPlayground {
  public static void main(String[] args) throws JsonProcessingException {
    ObjectMapper om = new ObjectMapper();
    Person p = new Person(11, "Alex");
    String json = om.writeValueAsString(p); // Getter
    // {"id":11,"name":"Alex"}
    System.out.println(json);

    String raw= """
        {"id":33,"name":"Sergio"}
        """;

    Person person = om.readValue(raw, Person.class);// NoArgConstructor + Setter
    // Person(id=33, name=Sergio)
    System.out.println(person);

  }
}
