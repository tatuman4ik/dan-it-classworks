package lesson41reflection.fruits;

import lesson41reflection.ann.Ripe;

@Ripe(40)
public class Plum implements Fruit {

  public String m3() {
    return "Plum";
  }

}
