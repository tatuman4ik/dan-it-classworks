package lesson41reflection.fruits;

import lesson41reflection.ann.Ripe;

@Ripe(30)
public class Apple implements Fruit{

  public String m1() {
    return "Apple";
  }

}
