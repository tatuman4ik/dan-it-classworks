package lesson41reflection.fruits;

import lesson41reflection.ann.Ripe;
import lesson41reflection.ann.Run;

@Ripe(90)
public class Watermelon implements Fruit {

  @Run
  public String m4() {
    return "Watermelon";
  }

}
