package lesson41reflection.fruits;

public class Lemon implements Fruit {

  @Override
  public String toString() {
    return "Lemon";
  }

}
