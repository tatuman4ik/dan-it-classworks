package lesson41reflection.fruits;

import lesson41reflection.ann.Ripe;
import lesson41reflection.ann.Run;

@Ripe(70)
public class Banana implements Fruit {

  @Run
  public String m2() {
    return "Banana";
  }

}
