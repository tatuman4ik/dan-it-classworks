package lesson41reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Playground {

  public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
    Box box1 = new Box(123);
    box1.print();

    String className = "lesson41reflection.Box";
    Class<?> aClass = Class.forName(className);
//    Constructor<?> constructor0 = aClass.getConstructor();
//    Object o = constructor0.newInstance();
    Constructor<?> constructor1 = aClass.getConstructor(int.class);
    Object o = constructor1.newInstance(456);
    Constructor<?>[] allConstructors = aClass.getConstructors();

    Box box2 = (Box) o;
    box2.print();

    Method method = aClass.getMethod("print2", String.class);
    method.invoke(o, ">>>>");
  }

}
