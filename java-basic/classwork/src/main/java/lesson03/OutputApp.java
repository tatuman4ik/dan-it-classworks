package lesson03;

import lesson04.Output2;
import libs.Console;

import static libs.Console.print;

public class OutputApp {

    public static void main(String[] args) {

        // no imports required
        Console.print("Hello");

        // import libs.Output2; - required;
        Output2.print("Hello");

        // import static libs.Output2.print; - required
        print("Hello");
    }

}
