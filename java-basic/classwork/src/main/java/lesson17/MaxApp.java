package lesson17;

import java.util.NoSuchElementException;
import java.util.Optional;

import static libs.EX.NI;

public class MaxApp {

  // no way to implement
  public static int max(int[] as) {
    throw NI;
  }

  // OK
  public static int max1(int[] as) throws NoSuchElementException {
    if (as.length == 0) throw new NoSuchElementException("max on empty array");
    int max = as[0];
    for (int a: as)
      max = Math.max(max, a);
    return max;
  }

  // OK
  /**
   * returns max element of .....
   *         null if dataset is empty
   */
  public static Integer max2(int[] as) {
    if (as.length == 0) return null;
    int max = as[0];
    for (int a: as)
      max = Math.max(max, a);
    return max;
  }

  // OK
  public static Optional<Integer> max3(int[] as) {
    if (as.length == 0) return Optional.empty();
    int max = as[0];
    for (int a: as)
      max = Math.max(max, a);
    return Optional.of(max);
  }

  public static void main(String[] args) {
    Optional<Integer> x1 = max3(new int[]{});
    Optional<Integer> x2 = max3(new int[]{1,7,6});

    System.out.println(x1);
    System.out.println(x2);
  }

}
