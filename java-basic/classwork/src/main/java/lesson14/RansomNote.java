package lesson14;

import lesson12.Counter;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;

public class RansomNote {

  public static Map<String, Integer> toMap1(List<String> words) {
    Counter<String> c = new Counter<>();
    for (String w : words) {
      c.count(w);
    }
    return c.get();
  }

  public static Map<String, Integer> toMap2(List<String> words) {
    Counter<String> c = new Counter<>();
    words.forEach(w -> c.count(w));
    return c.get();
  }

//  public static Map<String, Integer> toMapInt(List<String> words) {
//    words
//        .stream()
//        .collect(groupingBy(w -> w, counting()))
//        .entrySet() // Set<Entry<String, Long>>
//        .stream()   // Stream<Entry<String, Long>>
//        .map(wc -> {
//          String word = wc.getKey();
//          long count = wc.getValue();
//          int count2 = (int) count;
//          return new Map.Entry<String, Integer>() {
//            @Override
//            public String getKey() {
//              return word;
//            }
//
//            @Override
//            public Integer getValue() {
//              return count2;
//            }
//
//            @Override
//            public Integer setValue(Integer value) {
//              return null;
//            }
//
//            @Override
//            public boolean equals(Object o) {
//              return false;
//            }
//
//            @Override
//            public int hashCode() {
//              return 0;
//            }
//          };
//        })
//        .
//
//    return collect1;
//  }

  public static Map<String, Long> toMap3(List<String> words) {
    Map<String, Long> collect1 = words
        .stream()
        .collect(groupingBy(w -> w, counting()));

    Map<String, List<String>> collect2 = words
        .stream()
        .collect(groupingBy(w -> w));

    return collect1;
  }

  public static boolean checkMagazine1(List<String> magazine, List<String> note) {
    Map<String, Long> mapMagazine = toMap(magazine);
    Map<String, Long> mapNote = toMap(note);

    Set<String> wordsNote = mapNote.keySet();

    for (String word : wordsNote) {
      long noteCount = mapNote.get(word); // >0
      long magazineCount = mapMagazine.getOrDefault(word, 0L);
      if (noteCount < magazineCount) return false;
    }

    return true;
  }

  public static Map<String, Long> toMap(List<String> words) {
    return words
        .stream()
        .collect(groupingBy(w -> w, counting()));
  }

  public static boolean checkMagazine0(List<String> magazine, List<String> note) {
    Map<String, Long> iHave = toMap(magazine);
    Map<String, Long> iNeed = toMap(note);

    return iNeed.keySet().stream().allMatch(w ->
        iHave.getOrDefault(w, 0L) >= iNeed.get(w)
    );
  }

  public static void checkMagazine(List<String> magazine, List<String> note) {
    String r = checkMagazine0(magazine, note) ? "Yes" : "No";
    System.out.println(r);
  }


//
//
//    Set<String> wordsNote = iNeed.keySet();
//    for (String word: wordsNote) {
//      long noteCount = iNeed.get(word); // >0
//      long magazineCount = iHave.getOrDefault(word, 0L);
//      if (noteCount < magazineCount) return false;
//    }
//    return true;


}
