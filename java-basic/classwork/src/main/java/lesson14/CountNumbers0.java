package lesson14;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CountNumbers0 {

  public static Map<String, Long> countLetters(int min, int max) {
    String input = IntStream.rangeClosed(min, max)
        .mapToObj(n -> String.valueOf(n))
        .collect(Collectors.joining());

    Map<String, Long> output = input.codePoints()
        .mapToObj(c -> String.format("`%c`", c))
        .collect(Collectors.groupingBy(
            c -> c,
            Collectors.counting()
        ));

    return output;
  }

  public static void main(String[] args) {
    Map<String, Long> characterIntegerMap = countLetters(173, 215);
    System.out.println(characterIntegerMap);

  }
}
