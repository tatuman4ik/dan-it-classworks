package lesson14;

import java.util.stream.Stream;

public class MakeDigits {

  static Stream<Integer> digits(Integer n) {
    return Stream
        .iterate(n, i -> i / 10 > 0 || i % 10 > 0, i -> i / 10)
        .map(i -> i % 10);
  }

  public static void main(String[] args) {
    System.out.println(digits(51634).toList());
  }
}
