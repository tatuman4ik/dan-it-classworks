package lesson14;

import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CountNumbers2 {

  public static Map<String, Integer> countLetters(int min, int max) {
    return IntStream.rangeClosed(min, max)
        .mapToObj(String::valueOf)
        .flatMap(s -> s.chars().boxed())
        .map(c -> String.format("`%c`", c))
        .collect(Collectors.toMap(
            c -> c,
            x -> 1,
            (a, b) -> a + b,
            () -> new TreeMap<>()
        ));
  }

  public static void main(String[] args) {
    Map<String, Integer> characterIntegerMap = countLetters(173, 215);
    System.out.println(characterIntegerMap);

  }
}
