package libs;

public class CharUtils {

    public static boolean isNumber(char c) {
        return c >= '0' && c <= '9';
    }

    public static boolean isLower(char c) {
        return c >= 'a' && c <= 'z';
    }

    public static boolean isUpper(char c) {
        return c >= 'A' && c <= 'Z';
    }

    public static boolean isLetter(char c) {
        return isLower(c) || isUpper(c);
    }

    public static boolean isAlphaNumeric(char c) {
        return isLetter(c) || isNumber(c);
    }

}
