package lesson15;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileWrite {

  public static void main1(String[] args) throws IOException {
    String line = "Hello, World!";
    String fileName = "java-basic/classwork/src/main/java/lesson15/message.txt";
    // -----------------------------
    File file = new File(fileName);
    boolean exists = file.exists();
    boolean isDir = file.isDirectory();
    File absolute = file.getAbsoluteFile(); // /Users/alexr/dev/danit/fs-4-online/message.txt
    System.out.println(absolute);
    FileWriter fw = new FileWriter(file);
    fw.write(line);
    fw.close();
  }

  public static void main(String[] args) throws IOException {
    String line = "Hello, World!";
    String fileName = "java-basic/classwork/src/main/java/lesson15/message.txt";
    // -----------------------------
    File file = new File(fileName);
    boolean exists = file.exists();
    boolean isDir = file.isDirectory();
    File absolute = file.getAbsoluteFile(); // /Users/alexr/dev/danit/fs-4-online/message.txt
    FileWriter fw = new FileWriter(file);
    BufferedWriter bw = new BufferedWriter(fw, 16384);
    bw.write(line);
    bw.close();
  }

}
