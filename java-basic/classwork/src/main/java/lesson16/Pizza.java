package lesson16;

import lesson16.solution.Identifable;

public record Pizza(int id, String name, int size) implements Identifable { }