package lesson16.problem;

import lesson16.Pizza;
import lesson16.Student;

/**
 * DAO - Data Access Object
 *
 * - save
 * - load
 * - update
 * - delete
 */
public class DAOApp {

  public static void main(String[] args) {
    Pizza p1 = new Pizza(1, "Margarita", 30);
    Student s1 = new Student(1,"Jim", "FS1");

    DAOPizzaFile dao = new DAOPizzaFile();
    dao.save(p1);
    Pizza loaded = dao.load(1);
    dao.delete(1);

  }

}
