package lesson16;

import lesson16.solution.IdentifableSerializable;

public record Student(int id, String name, String group) implements IdentifableSerializable { }