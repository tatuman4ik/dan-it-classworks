package lesson16;

import java.util.ArrayList;
import java.util.List;

public class CombineApp {

  record Sentence(String subject, String verb, String object) {

    static Sentence of(String subject, String verb, String object) {
      return new Sentence(subject, verb, object);
    }

    @Override
    public String toString() {
      return String.format("%s %s %s", subject, verb, object);
    }
  }

  static List<Sentence> combinations1(List<String> subjects, List<String> verbs, List<String> objects) {
    ArrayList<Sentence> outcome = new ArrayList<>();
    for (String subj : subjects)
      for (String verb : verbs)
        for (String obj : objects)
          outcome.add(Sentence.of(subj, verb, obj));
    return outcome;
  }

  static List<Sentence> combinations2(List<String> subjects, List<String> verbs, List<String> objects) {
    ArrayList<Sentence> outcome = new ArrayList<>();
    subjects.stream().flatMap(subj ->
            verbs.stream().flatMap(verb ->
                objects.stream().map(obj ->
                    Sentence.of(subj, verb, obj)
                )
            )
        )
        .forEach(outcome::add);
    return outcome;
  }

  static List<Sentence> combinations(List<String> subjects, List<String> verbs, List<String> objects) {
    return subjects.stream().flatMap(subj ->
            verbs.stream().flatMap(verb ->
                objects.stream().map(obj ->
                    Sentence.of(subj, verb, obj)
                )
            )
        )
        .toList();
  }

  public static void main(String[] args) {
    List<String> subjects = List.of("Noel", "The cat", "The dog");
    List<String> verbs = List.of("wrote", "chased", "slept on");
    List<String> objects = List.of("the book", "the ball", "the bed");
    combinations(subjects, verbs, objects)
        .forEach(System.out::println);

    /**
     * Noel wrote the book
     * Noel wrote the ball
     * Noel wrote the bed
     * Noel chased the book
     * Noel chased the ball
     * Noel chased the bed
     * Noel slept on the book
     * Noel slept on the ball
     * Noel slept on the bed
     * The cat wrote the book
     * The cat wrote the ball
     * The cat wrote the bed
     * The cat chased the book
     * The cat chased the ball
     * The cat chased the bed
     * The cat slept on the book
     * The cat slept on the ball
     * The cat slept on the bed
     * The dog wrote the book
     * The dog wrote the ball
     * The dog wrote the bed
     * The dog chased the book
     * The dog chased the ball
     * The dog chased the bed
     * The dog slept on the book
     * The dog slept on the ball
     * The dog slept on the bed
     */

  }
}
