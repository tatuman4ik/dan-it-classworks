package lesson16.solution;

import java.sql.Connection;

public class DaoDatabase<A extends Identifable> implements DAO<A> {

  private final Connection connection;

  public DaoDatabase(Connection connection) {
    this.connection = connection;
  }

  @Override
  public void save(A a) {

  }

  @Override
  public A load(int id) {
    return null;
  }

  @Override
  public void delete(int id) {

  }
}
