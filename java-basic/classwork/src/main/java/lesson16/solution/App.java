package lesson16.solution;

import lesson16.Pizza;
import lesson16.Student;

import java.io.File;
import java.sql.DriverManager;

public class App {

  public static void main(String[] args) throws Exception {
//    Pizza p1 = new Pizza(1, "Margarita", 30);

//    DAO<Pizza> dao = new DaoHashMap<>();
//    dao.save(p1);
//    Pizza loaded = dao.load(1);
//    dao.delete(1);

    Student s1 = new Student(123, "JIM", "FS1");
    Student s2 = new Student(124, "JACK", "FS1");

    DAO<Student> dao2a = new DaoHashMap<>();
    DAO<Student> dao2 = new DaoFile<>(new File("students.bin"));
//    DAO<Student> dao2c = new DaoDatabase<>(DriverManager.getConnection("..."));
    dao2.save(s1);
    dao2.save(s2);
    Student loaded1 = dao2.load(123);
    Student loaded2 = dao2.load(124);
    System.out.println(loaded1);
    System.out.println(loaded2);
//    dao2.delete(123);
//    dao2.delete(s1);
  }

}
