package lesson16.solution;

public interface Identifable {

  int id();

}
