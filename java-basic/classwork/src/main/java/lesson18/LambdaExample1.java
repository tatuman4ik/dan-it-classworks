package lesson18;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class LambdaExample1 {

  interface MyWhatever {
    void please(int x);
  }

  static void myPrint1(Integer x) {
    System.out.println(x);
  }

  static void myPrint2(Integer x, String prefix, String suffix) {
    System.out.println(prefix);
    System.out.println(x);
    System.out.println(suffix);
  }

  public static void main(String[] args) {

    Consumer<Integer> ci = LambdaExample1::myPrint1;
    MyWhatever w = LambdaExample1::myPrint1;
    Consumer<Integer> ci2 = x -> w.please(x);

    Stream.of(1,2,3)
        .forEach(new Consumer<Integer>() {
          @Override
          public void accept(Integer x) {
            System.out.println(x);
          }
        });

    Stream.of(1,2,3)
        .forEach((Integer x) -> System.out.println(x));

    Stream.of(1,2,3)
        .forEach(x -> System.out.println(x));

    Stream.of(1,2,3)
        .forEach(System.out::println);

    Stream.of(1,2,3)
        .forEach(x -> myPrint1(x));

    Stream.of(1,2,3)
        .forEach(ci);

    Stream.of(1,2,3)
        .forEach(x -> w.please(x));

    Stream.of(1,2,3)
        .forEach(w::please);

    Stream.of(1,2,3)
        .forEach(LambdaExample1::myPrint1);

    Stream.of(1,2,3)
        .forEach(x -> myPrint2(x, "====", "===="));


  }

}
