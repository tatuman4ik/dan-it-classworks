package lesson18;

public class ExceptionApp1 {

  static int max(int[] xs) {
    if (xs.length == 0) {
      // it's not my business
      throw new RuntimeException("array is empty");
    }
    int max = xs[0];
    for (int x : xs) {
      if (x > max) max = x;
    }
    return max;
  }

  static String doSomething(int[] xs)  {
    try {
      int x = max(xs);
      return String.format("The max element of the array is %s", x);
    } catch (IllegalArgumentException ex) {
      return "The given array is empty!";
    }
  }

  public static void main(String[] args) {
    String message = doSomething(new int[]{});
    System.out.println(message);
  }

}
