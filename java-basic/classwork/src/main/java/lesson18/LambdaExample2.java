package lesson18;

import java.util.function.Function;

public class LambdaExample2 {

  public static void main(String[] args) {
    {
      int a = 5;
      int x = 3 + a;
      int y = x * 2;
    }
    {
      int a = 7;
      // f: Int => Int
      int x = 4 + a;
      // g: Int => Int
      int y = x * 3;
    }

    Function<Integer, Integer> f = a -> a + 3;
    Function<Integer, Integer> g = b -> b * 3;
    Function<Integer, Integer> h = f.andThen(g);
    Function<Integer, Integer> h2 = g.andThen(f);

    int z = h.apply(10); // (10 + 3) * 3
    System.out.println(z);

    int y = h2.apply(10); // (10 * 3) + 3
    System.out.println(y);
  }

}
