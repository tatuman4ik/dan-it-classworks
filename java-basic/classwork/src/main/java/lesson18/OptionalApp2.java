package lesson18;

import java.sql.Connection;
import java.util.Optional;

import static libs.EX.NI;

public class OptionalApp2 {

  class Request {}
  class Response {}
  class Header {}
  class Token {}
  class User {}
  class APIClient{}

  static Optional<Header> getHeader(Request rq) { throw NI; }
  static Optional<Token> getToken(Header h) { throw NI; }
  static Optional<User> getUser(Token t) { throw NI; }

  static Optional<User> process(Request rq) {
    return getHeader(rq)
        .flatMap(h -> getToken(h))
        .flatMap(t -> getUser(t));
  }

  public Response whatever(Request rq, Connection db, APIClient api) {
    // process
    // combine result
    // build a response
    throw NI;
  }

}
