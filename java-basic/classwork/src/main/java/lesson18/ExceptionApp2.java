package lesson18;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class ExceptionApp2 {

  static void openFile() throws FileNotFoundException {
    FileOutputStream fs = new FileOutputStream(new File("1.txt"));
  }

  static String doSomething() throws FileNotFoundException {
    openFile();
    return "OK";
  }

  public static void main(String[] args) throws FileNotFoundException {
    String message = doSomething();
    System.out.println(message);
  }

}
