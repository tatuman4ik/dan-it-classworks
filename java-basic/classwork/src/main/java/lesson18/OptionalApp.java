package lesson18;

import java.util.Optional;
import java.util.stream.Stream;

public class OptionalApp {

  static Optional<Integer> max(int[] xs) {
    if (xs.length == 0) return Optional.empty();
    int max = xs[0];
    for (int x : xs) {
      if (x > max) max = x;
    }
    return Optional.of(max);
  }

  void demo() {
    // BAD
    record Person1(String name, String surname){}
    new Person1("Jim", null);
    new Person1("Jim", "");
    // GOOD
    record Person2(String name, Optional<String> surname){}
    new Person2("Jim", Optional.of("Ben"));
    new Person2("Jim", Optional.empty());
  }

  static Optional<String> process1(Optional<Integer> value) {
    return value
        .map(x -> String.format("The max element is: %d", x))
        .map(x -> x.toUpperCase());
  }

  static String process2(Optional<String> value) {
    return value.orElse("The given array is empty!");
  }

  public static void main(String[] args) {
    Optional<Integer> max1 = max(new int[]{});
    Optional<Integer> max2 = max(new int[]{1, 2, 3});
    System.out.println(max1); // empty
    System.out.println(max2); // 3

    System.out.println(process2(process1(max1)));
    System.out.println(process2(process1(max2)));

    // VERY VERY BAD
    boolean present = max2.isPresent();
    Integer integer = max2.get();
//    Integer integer1 = max1.get(); // Exception
    // GOOD
    Optional<Integer> first = Stream.of(1, 2, 3)
        .filter(x -> x > 100)
        .findFirst();
  }

}
