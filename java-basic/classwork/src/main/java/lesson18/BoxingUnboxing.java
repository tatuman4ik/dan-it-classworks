package lesson18;

import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class BoxingUnboxing {

  public static void whatever1(int x) {}

  public static void whatever2(Integer x) {}

  public static void main(String[] args) {
    // autoboxing
    Integer x = 5;
    // autoboxing
    int a = 3;
    Integer a1 = a;
    // unboxing
    int a2 = a1;

    whatever1(a2); // 100% match
    whatever1(a1); // autounboxing
    whatever2(a1); // 100% match
    whatever2(a2); // autoboxing

    IntStream s2 = IntStream.of(1, 2, 3);
//    Stream<Integer> s1 =
    Integer[] array = Stream.of(1, 2, 3).toArray(Integer[]::new);

    String multiBefore15 = "Hello\n\"Alex\"\n";
    String multiSince15 = """
        Hello
        "Alex"
        """;
    String jsonBefore15 = "{\n  \"name\": \"Alex\",\n\"age\": 33\n}";
    String json15 = """
        {
          "name": "Alex",
          "age": 33
        }
        """;
    int w = 5;
    String r = switch (w) {
      case 1 -> "one";
      case 2 -> "two";
      default -> "else";
    };

    record Person(String name, int age) {}
    Person jim = new Person("Jim", 33);

    class A{}
    class A1 extends A{
      void hello(){};
    }
    class A2 extends A{}
    A f1 = new A1();
    // before 15
    if (f1 instanceof A1) {
      A1 aa1= (A1) f1;
      aa1.hello();
    }
    // after 15
    if (f1 instanceof A1) {
//      f1.hello();
    }


  }

}
