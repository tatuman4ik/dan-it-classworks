package lesson18;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.stream.Stream;

public class ExceptionApp3 {

  void doWriteNoEx(BufferedWriter bw, String s) {
    try {
      bw.write(s);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void main(String[] args) {
    BufferedWriter bw = new BufferedWriter(null);

    Stream.of("one", "two", "three")
        .forEach(x -> {
          try {
            bw.write(x);
          } catch (IOException e) {
            throw new RuntimeException(e);
          }
        });

    ExceptionApp3 ea = new ExceptionApp3();

    Stream.of("one", "two", "three")
        .forEach(x -> ea.doWriteNoEx(bw, x));

  }


}
