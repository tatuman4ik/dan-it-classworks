package lesson06;

public class Problems {

    public static void doSomething(Pizza pizza) {
        // ???
//        pizza.size = 120;
    }

    public static void main(String[] args) {

//        new Pizza("A", 1, new String[]{"pepper"});

        Pizza p1 = Pizza.of(
                "Margaritha",
                60,
                new String[]{"pepper"}
        );

        Pizza p2 = Pizza.withoutAddons(
                "Margaritha",
                30
        );

        p1.printMe();
//        p2.printMe();

        System.out.println(p1);
        System.out.println(p2);


        doSomething(p1);

    }

}
