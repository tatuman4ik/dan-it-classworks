package lesson12;

public class CountingValleys {

    public static int countingValleys1(int steps, String path) {
        int curr = 0;
        int prev = 0;
        int counter = 0;
        for (char c: path.toCharArray()) {
            switch (c) {
                case 'U': prev = curr; curr += 1; break;
                case 'D': prev = curr; curr -= 1; break;
            }
            if (curr == 0 && prev < 0) counter++;
        }
        return counter;
    }

    public static int countingValleys2(int steps, String path) {
        int curr = 0;
        int counter = 0;
        for (char c: path.toCharArray()) {
            switch (c) {
                case 'D': curr -= 1; break;
                case 'U': curr += 1; if (curr == 0) counter++; break;
            }
        }
        return counter;
    }

}
