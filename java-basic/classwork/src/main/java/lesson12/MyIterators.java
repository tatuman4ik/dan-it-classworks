package lesson12;

import java.util.Iterator;

public class MyIterators {

    public static <A> Iterator<A> arrayIterator(A[] data) {

        return new Iterator<A>() {

            int index = 0;

            @Override
            public boolean hasNext() {
                return index < data.length;
            }

            @Override
            public A next() {
                A m = data[index++];
                index++;
                return m;
            }
        };

    }

}
