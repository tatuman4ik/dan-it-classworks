package lesson13;

public class Kangaroo {

    private static boolean isTrivial(int x1, int v1, int x2, int v2) {
        return x1==x2 && v1==v2;
    }

    private static boolean canBeSolved(int x1, int v1, int x2, int v2) {
        return (x1>x2 && v1<v2) || (x1<x2 && v1>v2);
    }

    private static boolean canMeet(int x1, int v1, int x2, int v2) {
        double a = (double) (x2-x1) / (v1-v2);
        return a == (int) a;
    }

    public static String kangaroo(int x1, int v1, int x2, int v2) {
        boolean r = isTrivial(x1, v1, x2, v2) ||
                (canBeSolved(x1, v1, x2, v2) && canMeet(x1, v1, x2, v2));
        return r ? "YES" : "NO";
    }

}
