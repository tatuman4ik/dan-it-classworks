package lesson13;

import java.time.*;

public class DateTimeApp {

    public static void main(String[] args) {

        LocalDate now = LocalDate.now();
        System.out.println(now);
        int year = now.getYear();
        Month month = now.getMonth();
        int dayOfMonth = now.getDayOfMonth();
        int dayOfYear = now.getDayOfYear();
        boolean leapYear = now.isLeapYear();
        LocalDate parsed = LocalDate.parse("2023-03-28");

        LocalDate d1 = LocalDate.of(2022, 1, 15);// 15.01.2022
        LocalDate d2 = d1.plusMonths(1);
        LocalDate d3 = d1.plusDays(30);

        int i = d1.compareTo(d2);
        boolean after = d1.isAfter(d2);
        boolean before = d1.isBefore(d2);

        LocalTime tNow = LocalTime.now();
        tNow.getHour();
        tNow.getMinute();
        tNow.getSecond();
        tNow.getNano();
        LocalTime t2 = tNow.plusMinutes(15);
        tNow.isBefore(t2);
        tNow.isAfter(t2);
        System.out.println(tNow);
        LocalTime.parse("21:54:43.647108");
        LocalTime.parse("21:54:43");
        LocalTime.parse("21:54");

        LocalDateTime localDateTime = now.atTime(tNow);
        System.out.println(localDateTime);


        long l = System.currentTimeMillis();
        long l1 = System.nanoTime();
        Instant now1 = Instant.now();
        System.out.println(l);
        System.out.println(l1);


//        localDateTime.atZone(ZoneOffset.of("EET"));
//
//
//
    }

}
