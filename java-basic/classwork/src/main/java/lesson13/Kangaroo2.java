package lesson13;

public class Kangaroo2 {

    public static String kangaroo(int x1, int v1, int x2, int v2) {
        int pos1 = x1;
        int pos2 = x2;
        int diff;
        int temp;
        do {
            diff = (pos1 > pos2) ? pos1 - pos2 : pos2 - pos1;
            pos1 += v1;
            pos2 += v2;
            temp = (pos1 > pos2) ? pos1 - pos2 : pos2 - pos1;
        } while (diff != 0 && diff > temp);

        return diff == 0 ? "YES" : "NO";
    }


}
