package lesson13;

import lesson10.Pizza;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PizzaTest2 {

    public static void main(String[] args) {
        Pizza13 p1 = new Pizza13("Mar", 60);
        Pizza13 p2 = new Pizza13("4s", 45);
        Pizza13 p3 = new Pizza13("Peperoni", 30);

        List<Pizza13> pizzas = new ArrayList<>();
        pizzas.add(p1);
        pizzas.add(p2);
        pizzas.add(p3);

        Comparator<Pizza13> compBySize = new Comparator<>() {
            @Override
            public int compare(Pizza13 o1, Pizza13 o2) {
                return o1.size - o2.size;
            }
        };

        Comparator<Pizza13> compBySize1 = (o1, o2) -> o1.size - o2.size;

        Comparator<Pizza13> compBySize2 = Comparator.comparingInt(o -> o.size);

        Comparator<Pizza13> compByName = new Comparator<>() {
            @Override
            public int compare(Pizza13 o1, Pizza13 o2) {
                return o1.name.compareTo(o2.name);
            }
        };

        Comparator<Pizza13> compByName1 = (Pizza13 o1, Pizza13 o2) -> o1.name.compareTo(o2.name);

        Comparator<Pizza13> compByName2 = (o1, o2) -> o1.name.compareTo(o2.name);

        //                                                 how to extract key
        Comparator<Pizza13> compByName3 = Comparator.comparing(o -> o.name);

        Collections.sort(pizzas, compBySize);
        System.out.println(pizzas);

        Collections.sort(pizzas, compByName);
        System.out.println(pizzas);

    }
}
