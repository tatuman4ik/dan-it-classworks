package lesson04;

import java.util.Scanner;

public class IfElse5B {

    public static String doRepresent(int x) {
        return x < 10 ?
                String.format("%d is less than 10", x) :
                String.format("%d is greater or equal than 10", x);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();

        String message = doRepresent(x);
        System.out.println(message);
    }

}
