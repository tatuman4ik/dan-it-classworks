package lesson04;

import java.util.Scanner;

import static libs.StringUtils.*;

public class IfElse4 {

    public static String up(String s) {
        System.out.println("destroy the universe");
        return s.toUpperCase();
    }


    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        int x = s.nextInt();
        String message = switch (x) {
            case 3  -> up("three");
            case 10,
                 11 -> "ten or eleven";
            case 50 -> "fifty";
            default -> String.format("else: %d\n", x);
        };
        System.out.println(message);
    }
}
