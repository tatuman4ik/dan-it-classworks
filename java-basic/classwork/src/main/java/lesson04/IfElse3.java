package lesson04;

import java.util.Scanner;

import static libs.StringUtils.isDouble;
import static libs.StringUtils.isInt;
import static libs.StringUtils.isLong;

public class IfElse3 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String line = s.nextLine();
        if (isInt(line)) {
            System.out.println("Integer was entered");
        } else if (isLong(line)) {
            System.out.println("Long was entered");
        } else if (isDouble(line)) {
            System.out.println("Double was entered");
        } else {
            System.out.println("non-Integer, non-Double (String) was entered");
        }
        System.out.println("going further");
    }
}
