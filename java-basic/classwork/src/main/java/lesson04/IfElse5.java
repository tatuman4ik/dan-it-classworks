package lesson04;

import java.util.Scanner;

public class IfElse5 {

    public static void doProcess(int x) {
        if (x < 10) {
            System.out.printf("%d is less than 10", x);
        } else {
            System.out.printf("%d is greater or equal than 10", x);
        }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();

        doProcess(x);
    }

}
