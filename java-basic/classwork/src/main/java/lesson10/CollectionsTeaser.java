package lesson10;

import java.util.ArrayList;
import java.util.HashSet;

public class CollectionsTeaser {

    public static void main(String[] args) {
        ArrayList<Integer> a1 = new ArrayList<Integer>();
        a1.add(11);
        a1.add(12);
        a1.add(12);
        a1.add(13);
//        a1.add("Jim");
//        a1.add(3.5);
//        a1.add(true);
        System.out.println(a1);

        HashSet<String> set = new HashSet<>();
        set.add("Java");
        set.add("Scala");
        set.add("Python");
        set.add("JavaScript");
        set.add("Java");
        System.out.println(set); // [Java, Scala, JavaScript, Python]
        HashSet<Integer> set2 = new HashSet<>(a1);
        System.out.println(set2); // [11, 12, 13]
        Pizza p1 = new Pizza("Mar", 30);
        Pizza p2 = new Pizza("Mar", 30);
        Pizza p3 = new Pizza("4s", 30);
        HashSet<Pizza> pizzas = new HashSet<>();
        pizzas.add(p1);
        pizzas.add(p2);
        pizzas.add(p3);
        System.out.println(p1.hashCode());
        System.out.println(p2.hashCode());
        System.out.println(p3.hashCode());



        System.out.println(pizzas);



    }


}
