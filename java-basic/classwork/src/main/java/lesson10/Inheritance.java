package lesson10;

public class Inheritance {

    public static abstract class Animal {
        public void go() {}
        public abstract void sound();
    }
    public static class Dog extends Animal {
        public void bark() {}
        @Override
        public void sound() {
            System.out.println("bark");
        }
    }
    public static class Cat extends Animal {
        public void meow() {}
        @Override
        public void sound() {
            System.out.println("meow");
        }
    }

    public static void main(String[] args) {
        //  left = right
        Animal a = new Animal() {
            @Override
            public void sound() {
                System.out.println("pshhhh");
            }
        };
        Dog d1 = new Dog();
        Animal d2 = new Dog();
        Object d3 = new Dog(); // anti-pattern
        var d4 = new Dog();

        // left side
        a.go();
        d1.go();
        d1.bark();
        d2.go();
//        d2.bark();
//        d3.go();
        d4.go();

        // right side
        ((Dog)d2).bark();
        Cat d3c = (Cat) d3; // exception
        ((Cat)d3).go(); // exception


        {
            System.out.println(a instanceof Animal);     // true
            System.out.println(d1 instanceof Animal);    // true
            System.out.println(d2 instanceof Animal);    // true
            System.out.println(d3 instanceof Animal);    // true
            System.out.println(d1 instanceof Dog);       // true
            System.out.println(d2 instanceof Dog);       // true
            System.out.println(d3 instanceof Dog);       // true
            System.out.println(d1 instanceof Object);    // true
            System.out.println(d2 instanceof Object);    // true
            System.out.println(d3 instanceof Object);    // true}
        }
    }
}
