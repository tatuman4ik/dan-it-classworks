package lesson54mt;

import lombok.SneakyThrows;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

record Person(int id, String name) {
}

public class ThreadPoolsCompletableFutures {

  public static CompletableFuture<Person> buildUser(int id, String name) {
    return CompletableFuture.supplyAsync(() -> {
      System.out.print("combining user... ");
      printThread();
      return new Person(id, name);
    });
  }

  public static CompletableFuture<Person> sendHttp(Person p) {
    return CompletableFuture.supplyAsync(() -> {
      System.out.print("sending user....");
      printThread();
      return p;
    });
  }

  static CompletableFuture<Integer> makeMul(Integer x) {
    return CompletableFuture.completedFuture(x * 10);
  }

  static void printThread() {
    System.out.printf("Thread %s\n", Thread.currentThread().getName());

  }

  public static void main1(String[] args) throws InterruptedException, ExecutionException {
    // math
    ExecutorService es1 = Executors.newFixedThreadPool(4);

    // 5s
    Supplier<Integer> s1 = () -> {
      try {
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
      return (int) (Math.random() * 1000);
    };

    // 2s
    Supplier<String> s2 = () -> {
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
      return "Jim";
    };

    CompletableFuture<Integer> f1 = CompletableFuture.supplyAsync(s1, es1);
    CompletableFuture<String> f2 = CompletableFuture.supplyAsync(s2, es1);
    f1.thenApply(x -> x * 2);
    CompletableFuture<Integer> f4 = f1.thenComposeAsync(x -> makeMul(x), es1);


    System.out.println("\nDone!");

    es1.shutdown();
  }

  @SneakyThrows
  public static void sleep(long time) {
    Thread.sleep(time);
  }


  public static void main(String[] args) throws ExecutionException, InterruptedException {

    int nCPU = Runtime.getRuntime().availableProcessors(); // 12
    // math ~ CPU
    ExecutorService es1 = Executors.newFixedThreadPool(4); //
    // I/O
    ExecutorService esIO = Executors.newFixedThreadPool(10000);

    CompletableFuture<Integer> idF = CompletableFuture.supplyAsync(() -> {
      long now=  System.currentTimeMillis();
      System.out.printf("ID started at %d\n", now);
      System.out.print("ID calculating id..");
      printThread();
      sleep(2000);
      System.out.printf("ID finished at at %d\n\n", System.currentTimeMillis()-now);
      return 15;
    }, es1);

    CompletableFuture<String> nameF = CompletableFuture.supplyAsync(() -> {
      long now=  System.currentTimeMillis();
      System.out.printf("NAME started at %d\n", now);
      System.out.print("NAME calculating name..");
      printThread();
      sleep(1000);
      System.out.printf("NAME finished at at %d\n\n", System.currentTimeMillis()-now);
      return "Jim";
    }, es1);

    //new Person(idF.get(), nameF.get())

    CompletableFuture<Void> app = nameF
        .thenComposeAsync(name ->
            idF.thenApply(x -> {
              System.out.print("adding 3..");
              printThread();
              return x + 3;
            }).thenComposeAsync(id ->
                buildUser(id, name),
                esIO
            )
        )
        .thenComposeAsync(p -> sendHttp(p), esIO)
        .thenAccept(person -> System.out.printf("Person %s sent", person))
        .exceptionally(t -> {
          System.out.println("something went wrong");
          return null;
        });

    app.get();

    esIO.shutdown();
    es1.shutdown();
  }



}
