package lesson54mt;


import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class Intro2AtomicLong {

  public static void main(String[] args) throws InterruptedException {
    AtomicInteger box = new AtomicInteger();

    CountDownLatch cdl = new CountDownLatch(2);

    Runnable r1 = () -> {
      System.out.printf("inc: Thread: %s\n", Thread.currentThread().getName());
      for (int i = 0; i < 10_000_000; i++) {
        box.incrementAndGet();
      }
      cdl.countDown();
    };

    Runnable r2 = () -> {
      System.out.printf("dec: Thread: %s\n", Thread.currentThread().getName());
      for (int i = 0; i < 10_000_000; i++) {
        box.decrementAndGet();
      }
      cdl.countDown();
    };

    Thread t1 = new Thread(r1);
    Thread t2 = new Thread(r2);
    long curr = System.currentTimeMillis();
    t1.start();
    t2.start();
    cdl.await();
    long now = System.currentTimeMillis();

    System.out.printf("Thread: %s\n", Thread.currentThread().getName());
    System.out.println(box.get());
    System.out.printf("Spent %d ms\n", now-curr); //  20 ms   - no synchronized
                                                  // 250 ms - with CompareAndSet (CAS)
                                                  // 480 ms - with synchronized
  }

}
