package lesson54mt;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class Atomic3 {

  public static void main(String[] args) throws InterruptedException {

    AtomicReference<Integer> total = new AtomicReference<>(0);

    Consumer<Integer> consumer = new Consumer<>() {
      @Override
      public void accept(Integer x) {
        total.accumulateAndGet(x, (curr, x0) -> curr + x0);
      }
    };

    Thread t1 = new Thread(() -> List.of(1, 2, 3).forEach(consumer));
    Thread t2 = new Thread(() -> List.of(11, 21, 31).forEach(consumer));

    t1.start();
    t2.start();
    Thread.sleep(100);
    System.out.println(total.get());

    total.accumulateAndGet(100, (x, y) -> x + y);

    System.out.println(total.get());

  }

}
