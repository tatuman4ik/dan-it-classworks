package lesson54mt;

class Box {
  int x;

  private final Object lock = new Object();

  public void inc() {
    //
    //
    //
    //
    synchronized (lock) {
      x++;
    }
    //
    //
    //
    //
  }

  public void dec() {
    //
    //
    //
    synchronized (lock) {
      x--;
    }
    //
    //
    //
  }

}
