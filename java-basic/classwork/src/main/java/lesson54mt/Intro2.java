package lesson54mt;


import java.util.concurrent.CountDownLatch;

public class Intro2 {

  public static void main(String[] args) throws InterruptedException {
    Box box = new Box();

    CountDownLatch cdl = new CountDownLatch(2);

    Runnable r1 = () -> {
      System.out.printf("inc: Thread: %s\n", Thread.currentThread().getName());
      for (int i = 0; i < 10000000; i++) {
        box.inc();
      }
      cdl.countDown();
    };

    Runnable r2 = () -> {
      System.out.printf("dec: Thread: %s\n", Thread.currentThread().getName());
      for (int i = 0; i < 10000000; i++) {
        box.dec();
      }
      cdl.countDown();
    };

    Thread t1 = new Thread(r1);
    Thread t2 = new Thread(r2);
    long curr = System.currentTimeMillis();
    t1.start();
    t2.start();
    cdl.await();
    long now = System.currentTimeMillis();

    System.out.printf("Thread: %s\n", Thread.currentThread().getName());
    System.out.println(box.x);
    System.out.printf("Spent %d ms\n", now-curr); //  20 ms   - no synchronized
                                                  // 480 ms - with synchronized
  }

}
