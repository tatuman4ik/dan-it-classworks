package lesson54mt;

import lombok.SneakyThrows;

import java.util.concurrent.*;

public class ThreadPoolsFutures {

  public static void main(String[] args) throws InterruptedException, ExecutionException {
    // math
    ExecutorService es1 = Executors.newFixedThreadPool(4);
    // I/O
    ExecutorService es2 = Executors.newFixedThreadPool(10000);

    CountDownLatch latch = new CountDownLatch(1);

    Runnable r1 = new Runnable() {
      @SneakyThrows
      @Override
      public void run() {
        for (int i = 0; i < 100; i++) {
          Thread.sleep(50);
          System.out.print(".");
        }
        latch.countDown();
      }
    };

    // 5s
    Callable<Integer> c1 = new Callable<>() {
      @Override
      public Integer call() throws Exception {
        Thread.sleep(5000);
        return (int) (Math.random() * 1000);
      }
    };

    Future<?> submitted1 = es1.submit(r1);
    Future<Integer> submitted2 = es1.submit(c1);

    // time waste !!!
    while (!submitted2.isDone()) {
      Thread.sleep(100);
    }
    int x = submitted2.get();

    latch.await();
    System.out.println("\nDone!");


    es2.shutdown();
    es1.shutdown();
  }

}
