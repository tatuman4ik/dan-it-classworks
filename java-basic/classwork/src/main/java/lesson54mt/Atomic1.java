package lesson54mt;

import java.util.List;
import java.util.function.Consumer;

public class Atomic1 {

  public static void main(String[] args) {

    final int[] total = {0};

    Consumer<Integer> consumer = new Consumer<>() {
      @Override
      public void accept(Integer x) {
        total[0] += x;
      }
    };

    // will fail if data relatively big
    Thread t1 = new Thread(() -> List.of(1, 2, 3).forEach(consumer));
    Thread t2 = new Thread(() -> List.of(11, 21, 31).forEach(consumer));

    t1.start();
    t2.start();
  }

}
