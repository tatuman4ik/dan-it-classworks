package lesson54mt;


public class Intro {

  public static void main(String[] args) throws InterruptedException {
    Box box = new Box();
    Ready ready1 = new Ready();
    Ready ready2 = new Ready();

    Runnable r1 = () -> {
      System.out.printf("inc: Thread: %s\n", Thread.currentThread().getName());
      for (int i = 0; i < 10000000; i++) {
        box.inc();
      }
      ready1.done();
    };

    Runnable r2 = () -> {
      System.out.printf("dec: Thread: %s\n", Thread.currentThread().getName());
      for (int i = 0; i < 10000000; i++) {
        box.dec();
      }
      ready2.done();
    };

    Thread t1 = new Thread(r1);
    Thread t2 = new Thread(r2);
    t1.start();
    t2.start();
    System.out.printf("Thread: %s\n", Thread.currentThread().getName());

    while (!(ready1.ready && ready2.ready)) {
      Thread.sleep(0, 500);
    }

    System.out.println(box.x);
  }

}
