package lesson08;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Lesson8Launcher {

    @Test
    public void test1() {
        int expected = 5;
        int real = 4;
        assertEquals(expected, real);
    }

    @Test
    public void test2() {
        assertEquals(1, 1);
    }

}
