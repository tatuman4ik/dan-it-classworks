package lesson08;

public class OrderStatusX {
    public final static OrderStatusX CREATED = new OrderStatusX();
    public final static OrderStatusX SHIPPED = new OrderStatusX();
    public final static OrderStatusX CANCELLED = new OrderStatusX();
    public final static OrderStatusX FINISHED = new OrderStatusX();

    private OrderStatusX() {}
}
