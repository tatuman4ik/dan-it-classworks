package lesson08;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;

public class Application {

    public static String CREATED = "CREATED";
    public static String SHIPPED = "SHIPPED";
    public static String CANCELLED = "CANCELLED";
    public static String FINISHED = "FINISHED";

    public void whateverBad(String orderStatus) {
        switch (orderStatus) {
            case "CREATED": // vs "СREATED"
                System.out.println("order is created");
        }
    }

    public void whateverGood(OrderStatus orderStatus) {
        switch (orderStatus) {
            case CREATED: System.out.println("order is created");
        }
    }

    public void whateverGood2(OrderStatusX orderStatus) {
        if (orderStatus.equals(OrderStatusX.CREATED)) {

        }
    }


    public static void main(String[] args) {
        Application app = new Application();
        app.whateverBad(SHIPPED);
        app.whateverBad("FINISHED");
        app.whateverBad("СREATED");

        app.whateverGood(OrderStatus.CREATED);
//        app.whateverGood(Application.CREATED);
//        app.whateverGood("CREATED");

        app.whateverGood2(OrderStatusX.CREATED);
//        app.whateverGood2(new OrderStatusX());

        LocalDate now = LocalDate.now();
        int year = now.getYear();
        Month month = now.getMonth();

    }


}
