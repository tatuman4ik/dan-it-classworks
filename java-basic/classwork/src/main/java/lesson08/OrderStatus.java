package lesson08;

public enum OrderStatus {
    CREATED,
    SHIPPED,
    CANCELLED,
    FINISHED
}

