package lesson05;

import java.util.Arrays;

public class ArrayCopy {

    public static void main(String[] args) {
        int[] ints1 = {1,6,4,8,9};
        String asString = Arrays.toString(ints1);
        System.out.println(asString);

        int[] ints2 = new int[10];
        System.arraycopy(ints1, 0, ints2, 0, 5);
        System.arraycopy(ints1, 0, ints2, 5, 5);
        System.out.println(Arrays.toString(ints2));

        int[] ints3 = Arrays.copyOf(ints1, 3);
        System.out.println(Arrays.toString(ints3));

        int[] ints10 = Arrays.copyOf(ints1, 10);
        System.out.println(Arrays.toString(ints10));

    }

}
