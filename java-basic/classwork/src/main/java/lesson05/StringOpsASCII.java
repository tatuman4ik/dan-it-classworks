package lesson05;

import java.nio.charset.StandardCharsets;

public class StringOpsASCII {

    public static void main(String[] args) {
        String s0 = "hello";
        String s1 = Character.toUpperCase(s0.charAt(0))
                + s0.substring(1);

        System.out.println(s1);

        byte[] bytes = s0.getBytes(StandardCharsets.UTF_8);
        bytes[0] = (byte) Character.toUpperCase(bytes[0]);
        String s2 = new String(bytes);
        System.out.println(s2);
    }

}
