package lesson05;

public class NestedArrays {

    public static void main1(String[] args) {

        int[][] ints = new int[5][3];

        int[] line1 = ints[0];

        int col1 = line1[0];
        int col2 = line1[1];
        int col3 = line1[2];

        int[] line2 = ints[1];
        int[] line3 = ints[2];
        int[] line4 = ints[3];
        int[] line5 = ints[4];


    }

    public static void main2(String[] args) {
        int[][] xs = {
                {1, 2, 3},
                {4, 5},
                {6},
                {},
                {7, 8, 9, 10}
        };
    }

    public static void main(String[] args) {
        int[][][][] ints = new int[10][20][30][40];
    }

}
