package app.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Book {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "b_id")
  Integer id;

  @Column(name = "b_name")
  String name;

  @ManyToMany
  Set<Author> authors;

}
