package app.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Phone {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "p_id")
  Integer id;

  @Column(name = "p_number")
  String number;

}
