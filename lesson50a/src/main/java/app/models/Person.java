package app.models;

public record Person(Integer id, String name) {
}
