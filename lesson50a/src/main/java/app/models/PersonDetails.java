package app.models;

public record PersonDetails(Integer id, String name) {
}
