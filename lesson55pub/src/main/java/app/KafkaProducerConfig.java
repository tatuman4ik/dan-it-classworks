package app;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.HashMap;

@Configuration
public class KafkaProducerConfig {

  @Bean
  public KafkaTemplate<String, String> makeTemplate(KafkaProperties props) {
    HashMap<String, Object> c = new HashMap<>(props.getProducerProperties());
    c.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, props.getBootstrapServers());

    DefaultKafkaProducerFactory<String, String> f =
        new DefaultKafkaProducerFactory<>(c);

    return new KafkaTemplate<>(f);
  }

}
