package app;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AppController {

  //                             K      V
  private final KafkaTemplate<String, String> kafkaTemplate;
  private final KafkaProperties kafkaProperties;

  @GetMapping("send/{id}/{message}")
  public String handle(@PathVariable("id") Integer id,
                       @PathVariable("message") String message) {
    kafkaTemplate.send(
        kafkaProperties.getTopic(), // topic
        id.toString(),              // key
        message                     // value
    ).addCallback(
        new ListenableFutureCallback<SendResult<String, String>>() {

          @Override
          public void onSuccess(SendResult<String, String> result) {
            System.out.printf("offset: %d", result.getRecordMetadata().offset());
          }

          @Override
          public void onFailure(Throwable ex) {
          }
        }


//        (SendResult<String, String> x) ->
//            System.out.println(x)

        );

    return String.format("`%s` sent", message);
  }

}
