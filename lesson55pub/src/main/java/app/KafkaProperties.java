package app;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
@Data
@ConfigurationProperties("kafka")
public class KafkaProperties {
  private String topic;
  private String bootstrapServers;
  private Map<String, String> producerProperties;
}
