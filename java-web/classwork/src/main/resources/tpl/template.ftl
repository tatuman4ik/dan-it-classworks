<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <b>Hello ${name}</b>

    <table border="1">
        <thead>
            <td>id</td>
            <td>name</td>
            <td>price</td>
        </thead>
        <tbody>
        <#list items as line>
            <tr>
                <td>${line.id}</td> <#-- getter syntax, NOT A FIELD -->
                <td>${line.name}</td>
                <td>${line.price}</td>
            </tr>
            <tr>
                <td>${line.getId()}</td> <#-- method call syntax -->
                <td>${line.getName()}</td>
                <td>${line.getPrice()}</td>
            </tr>
            <tr>
                <td>${line.getId()}</td> <#-- method call syntax -->
                <td>${line.getNameUpperCase()}</td>
                <td>${line.getPrice()}</td>
            </tr>
        </#list>
        </tbody>
    </table>
</body>
</html>
