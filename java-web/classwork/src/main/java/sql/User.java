package sql;

public record User(Integer id, String name) implements Identifable {

  static User noId(String name) {
    return new User(null, name);
  }

}
