package sql;

import java.sql.*;

public class AppDAO {

  private static final String[] names = {"Alex", "Jim", "Serg", "Nate", "Nikita", "Sasha", "Petra"};
  private static final int defaultPageSize = 20;

  private static String name() {
    return names[(int) (Math.random() * 4)];
  }

  record Page(int number) {
    public String toSQL() {
      return String.format(
          """
              OFFSET %d
              LIMIT %d
              """, (number-1)*defaultPageSize, defaultPageSize);
    }
  }

  public static void main(String[] args) throws Exception {
    Connection conn = Database.conn();

//    DAO<User> userDAO = new UserDAO(conn);
//
//    IntStream.range(10,100).forEach(x -> {
//      try {
//        userDAO.save(User.noId(name()));
//      } catch (Exception e) {
//        throw new RuntimeException(e);
//      }
//    });

    Page page = new Page(2);
    System.out.println(page.toSQL());


//    System.out.println(userDAO.load(1));
//    System.out.println(userDAO.load(11));
//    userDAO.save(new User(null, "Alehandro"));
//    userDAO.save(new User(11, "Alehandro".toUpperCase()));
//    userDAO.delete(11);
  }

}
