package sql;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

  private static final String url = "jdbc:postgresql://localhost:5432/fs4online";
  private static final String user = "postgres";
  private static final String pass = "pg123456";

  /** analyze current structure & apply deltas */
  public static void checkAndApplyDeltas() {
    FluentConfiguration conf = new FluentConfiguration()
        .dataSource(url, user, pass);
    Flyway flyway = new Flyway(conf);
    flyway.migrate();
  }

  public static Connection conn() throws SQLException {
    return DriverManager.getConnection(url, user, pass);
  }

}
