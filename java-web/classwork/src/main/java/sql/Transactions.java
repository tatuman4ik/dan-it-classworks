package sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.sql.Connection.*;

public class Transactions {

  private final Connection conn;

  public Transactions(Connection conn) {
    this.conn = conn;
  }

  public void blockWithPotentialProblem() throws SQLException {
    PreparedStatement st1 = conn.prepareStatement("UPDATE users SET salary = 110 where id = 5");
    PreparedStatement st2 = conn.prepareStatement("UPDATE users SET salary = 90 where id = 6");

    // conn.setAutoCommit(true)
    // by default
    st1.executeUpdate(); // written immediately
    st2.executeUpdate(); // written immediately
  }

  public void blockWithoutPotentialProblem() throws SQLException {
    PreparedStatement st1 = conn.prepareStatement("UPDATE users SET salary = 110 where id = 5");
    PreparedStatement st2 = conn.prepareStatement("UPDATE users SET salary = 90 where id = 6");

    conn.setAutoCommit(false);
    try {
      st1.executeUpdate();
      st2.executeUpdate();
      conn.commit(); // both written here
    } catch (Exception x ) {
      // log error
      // message
      conn.rollback();
    }

  }

  private void runOne(PreparedStatement st) {
    try {
      st.execute();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public void transitionally(Stream<PreparedStatement> stmts) throws SQLException {
    conn.setAutoCommit(false);
    try {
      stmts.forEach(this::runOne);
      conn.commit(); // ALL written here
    } catch (Exception x ) {
      // log error
      // message
      conn.rollback();
    }
  }

  public void transitionally(Iterable<PreparedStatement> stmts) throws SQLException {
    transitionally(StreamSupport.stream(stmts.spliterator(), false));
  }

  public void transitionally(PreparedStatement... stmts) throws SQLException {
    transitionally(Arrays.stream(stmts));
  }

  public void isolation() throws SQLException {
    conn.setTransactionIsolation(TRANSACTION_READ_UNCOMMITTED);
    conn.setTransactionIsolation(TRANSACTION_READ_COMMITTED);
    conn.setTransactionIsolation(TRANSACTION_REPEATABLE_READ);
    conn.setTransactionIsolation(TRANSACTION_SERIALIZABLE);
  }

}
