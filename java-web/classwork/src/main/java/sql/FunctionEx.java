package sql;

public interface FunctionEx<A, B> {
  B apply(A a) throws Exception;
}
