package sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

public class UserDAO implements DAO<User> {

  private final Connection conn;

  public UserDAO(Connection conn) {
    this.conn = conn;
  }

  private void insert(User user) throws Exception{
    PreparedStatement stmt = conn.prepareStatement("INSERT INTO users (name) values (?)");
    stmt.setString(1, user.name());
    stmt.execute();
  }

  private void update(User user) throws Exception{
    PreparedStatement stmt = conn.prepareStatement("UPDATE users SET name = ? WHERE id = ?");
    stmt.setString(1, user.name());
    stmt.setInt(2, user.id());
    stmt.execute();
  }

  @Override
  public void save(User user) throws Exception {
    if (user.id() == null) insert(user);
    else                   update(user);
  }

  @Override
  public Optional<User> load(int id) throws Exception {
    PreparedStatement stmt = conn.prepareStatement("SELECT id, name FROM users WHERE id = ?");
    stmt.setInt(1, id);
    // 0 or 1 record
    ResultSet rs = stmt.executeQuery();
    if (rs.next()) {
      return Optional.of(new User(
          rs.getInt("id"),
          rs.getString("name")
      ));
    }
    return Optional.empty();
  }

  @Override
  public void delete(int id) throws Exception {
    PreparedStatement stmt = conn.prepareStatement("DELETE FROM users WHERE id = ?");
    stmt.setInt(1, id);
    stmt.executeUpdate();
  }
}
