package org.danit;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.Function;

public class CookieFilterV2 implements HttpFilter {

  private final Function<HttpServletRequest, Boolean> checkFn;
  private final ConsumerEx<HttpServletResponse> failedFn;

  public CookieFilterV2(Function<HttpServletRequest, Boolean> checkFn, ConsumerEx<HttpServletResponse> failedFn) {
    this.checkFn = checkFn;
    this.failedFn = failedFn;
  }

  @Override
  public void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
    if (checkFn.apply(request)) {
      // if Cookie is present
      chain.doFilter(request, response);
    } else {
      // if cookie is absent
      failedFn.accept(response);
    }
  }

}
