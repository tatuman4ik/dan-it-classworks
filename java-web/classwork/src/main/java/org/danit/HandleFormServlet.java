package org.danit;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class HandleFormServlet extends HttpServlet {

  // ShowFormServlet
  // http://localhost:8080/form
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
    cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
    cfg.setDirectoryForTemplateLoading(new File(ResourcesOps.dirUnsafe("tpl")));

    try (PrintWriter w = resp.getWriter()) {
      cfg
          .getTemplate("form.ftl")
          .process(new HashMap<String, Object>(), w);
    } catch (TemplateException x) {
      throw new RuntimeException(x);
    }
  }

  // ProcessFormServlet
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String username = req.getParameter("username");
    String password = req.getParameter("password");
    try (PrintWriter w = resp.getWriter()){
      w.printf("username: %s, password: %s", username, password);
    }
  }
}
