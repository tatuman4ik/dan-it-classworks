package org.danit;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import sql.Database;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import java.sql.Connection;
import java.util.EnumSet;
import java.util.Optional;

public class Launcher {

  static Optional<Integer> toInt(String raw) {
    try {
      return Optional.of(Integer.parseInt(raw));
    } catch (Exception x) {
      return Optional.empty();
    }
  }

  public static void main(String[] args) throws Exception {
    Integer port = Optional.ofNullable(System.getenv("PORT"))
        .flatMap(Launcher::toInt)
        .orElse(8080);

    Server server = new Server(port);

    Database.checkAndApplyDeltas();
    Connection conn = Database.conn();

    History history =
//        new HistoryInMemory();
        new HistoryInDb(conn);
    ServletContextHandler handler = new ServletContextHandler();

    String osStaticLocation = ResourcesOps.dirUnsafe("static-content");
    handler.addServlet(new ServletHolder(new StaticContentServlet(osStaticLocation)), "/static/*");
    handler.addServlet(new ServletHolder(new LoginServlet()), "/login");
    handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");
    handler.addServlet(new ServletHolder(new HandleFormServlet()), "/form");
    handler.addServlet(new ServletHolder(new FileDownloadServlet()), "/dl");

    ServletHolder sh = new ServletHolder(new FileUploadServlet());
    // upload only
    sh.getRegistration().setMultipartConfig(new MultipartConfigElement("./from-user"));
    handler.addServlet(sh, "/upload");

    Filter cookieFilter = HttpFilter.make(
        rq -> Auth.tryGetCookie(rq).isPresent(),
        rs -> rs.sendRedirect("https://google.com")
    );
    EnumSet<DispatcherType> dt = EnumSet.of(DispatcherType.REQUEST);

    Filter loggerFilter = HttpFilter.prefix(
        rq -> System.out.println(rq.getRequestURL())
    );

    handler.addFilter(new FilterHolder(loggerFilter), "/calc", dt);
    handler.addFilter(new FilterHolder(cookieFilter), "/calc", dt);

    Filter checkParamFilter = HttpFilter.make(
        rq -> rq.getParameter("x") != null && rq.getParameter("y") != null,
        rs -> rs.setStatus(400)
    );

    handler.addFilter(new FilterHolder(checkParamFilter), "/calc", dt);

    handler.addServlet(new ServletHolder(new CalculatorServlet(history)), "/calc");
//    handler.addFilter(MyFilter.class, "/history", EnumSet.of(DispatcherType.REQUEST));
    handler.addFilter(new FilterHolder(cookieFilter), "/history", dt);
    handler.addServlet(new ServletHolder(new HistoryServlet(history)), "/history");
//    handler.addServlet(SumServlet1.class, "/sum1");
//    handler.addServlet("org.danit.SumServlet1", "/sum1");
    handler.addServlet(new ServletHolder(new SumServlet1()), "/sum1");
    handler.addServlet(new ServletHolder(new SumServlet2()), "/sum2");
    handler.addServlet(new ServletHolder(new SumServlet3()), "/sum3");
    handler.addServlet(new ServletHolder(new HtmlPageServlet()), "/html");
    handler.addServlet(new ServletHolder(new BinaryContentServlet()), "/bin");
    handler.addServlet(new ServletHolder(new TemplateServlet()), "/template");
//    handler.addServlet(new ServletHolder(new HelloServlet("Hello from Java")), "/");
//    handler.addServlet(HelloServlet.class, "/");
//    handler.addServlet("org.danit.HelloServlet", "/");

    server.setHandler(handler);

    server.start();
    server.join();
  }

}