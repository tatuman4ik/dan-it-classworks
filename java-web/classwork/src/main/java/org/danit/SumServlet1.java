package org.danit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

public class SumServlet1 extends HttpServlet {

  // http://localhost:8080/sum1?a=3&b=4&c=5
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Map<String, String[]> allParameters = req.getParameterMap();
    StringBuilder sb = new StringBuilder();
    int z = 0;
    for (String key: allParameters.keySet()) {
      String p = allParameters.get(key)[0];
      if (!sb.isEmpty()) sb.append(" + ");
      sb.append(p);
      z = z + Integer.parseInt(p);
    }
    try (Writer w = resp.getWriter()) {
      w.write(String.format("%s = %d", sb, z));
    }
  }

}
