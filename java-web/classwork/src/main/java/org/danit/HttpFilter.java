package org.danit;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Function;

public interface HttpFilter extends Filter {
  @Override
  default void init(FilterConfig filterConfig) {}

  private boolean isHttp(ServletRequest request0,
                         ServletResponse response0) {
    return request0 instanceof HttpServletRequest
        && response0 instanceof HttpServletResponse;
  }

  void doHttpFilter(HttpServletRequest request,
                    HttpServletResponse response,
                    FilterChain chain
                    ) throws IOException, ServletException;

  @Override
  default void doFilter(
      ServletRequest rq0,
      ServletResponse rs0,
      FilterChain chain) throws IOException, ServletException {

    if (isHttp(rq0, rs0)) {
      HttpServletRequest request = (HttpServletRequest) rq0;
      HttpServletResponse response = (HttpServletResponse) rs0;

      doHttpFilter(request, response, chain);
    } else {
      // if non-http request
      chain.doFilter(rq0, rs0);
    }

  }

  @Override
  default void destroy() {}

  static HttpFilter prefix(Consumer<HttpServletRequest> preFn) {
    return new HttpFilter() {
      @Override
      public void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        preFn.accept(request);
        chain.doFilter(request, response);
      }
    };
  }

  static HttpFilter make(
      Function<HttpServletRequest, Boolean> checkFn,
      ConsumerEx<HttpServletResponse> failedFn
  ) {

    return new HttpFilter() {
      @Override
      public void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (checkFn.apply(request)) {
          // if Cookie is present
          chain.doFilter(request, response);
        } else {
          // if cookie is absent
          failedFn.accept(response);
        }
      }
    };
  }

}
