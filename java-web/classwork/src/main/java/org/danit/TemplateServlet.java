package org.danit;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

public class TemplateServlet extends HttpServlet {

  // http://localhost:8080/template
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
    cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
    cfg.setDirectoryForTemplateLoading(new File(ResourcesOps.dirUnsafe("tpl")));

    HashMap<String, Object> data = new HashMap<>();
    data.put("name", "JIM");

    ArrayList<PriceLine> items = new ArrayList<>();
    items.add(new PriceLine(1, "Iphone 5", 200.2));
    items.add(new PriceLine(2, "<b>Iphone X</b>", 500.5));
    items.add(new PriceLine(3, "Iphone 13Pro", 1300.13));
    data.put("items", items);

    try (PrintWriter w = resp.getWriter()) {
      cfg
          .getTemplate("template.ftl")
          .process(data, w);
    } catch (TemplateException x) {
      throw new RuntimeException(x);
    }
  }

}
