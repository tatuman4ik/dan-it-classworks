package org.danit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

public class SumServlet2 extends HttpServlet {

  // http://localhost:8080/sum2?a=3&a=4&a=5
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Map<String, String[]> allParameters = req.getParameterMap();
    String[] as = allParameters.get("a");

    StringBuilder sb = new StringBuilder();
    int z = 0;
    for (String a: as) {
      if (!sb.isEmpty()) sb.append(" + ");
      sb.append(a);
      z = z + Integer.parseInt(a);
    }
    try (Writer w = resp.getWriter()) {
      w.write(String.format("%s = %d", sb, z));
    }
  }

}
