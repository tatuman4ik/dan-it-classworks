package org.danit;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Map;

public class SumServlet3 extends HttpServlet {

  // http://localhost:8080/sum3?a=3,4,5,1000
  // http://localhost:8080/sum3/qqq/www/eee?a=3,4,5,1000
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    System.out.println(req.getQueryString()); // a=3,4,5,1000
    System.out.println(req.getProtocol());    // HTTP/1.1 [s]
    System.out.println(req.getMethod());      // GET
    System.out.println(req.getRequestURI());  // /sum3
    System.out.println(req.getPathInfo());    //
    System.out.println(req.getRequestURL());  // http://localhost:8080/sum3
    System.out.println(req.getServletPath()); // /sum3
    System.out.println(req.getRemoteAddr());  // [0:0:0:0:0:0:0:1]
    System.out.println(req.getRemoteHost());  //
    System.out.println(req.getHeaderNames()); //
    System.out.println(Arrays.toString(req.getCookies()));  //
    System.out.println(req.getParts());       //
    ServletInputStream stream = req.getInputStream();
    byte[] body = stream.readAllBytes();


    Map<String, String[]> allParameters = req.getParameterMap();
    String[] as = allParameters.get("a")[0].split(",");

    StringBuilder sb = new StringBuilder();
    int z = 0;
    for (String a: as) {
      if (!sb.isEmpty()) sb.append(" + ");
      sb.append(a);
      z = z + Integer.parseInt(a);
    }
    try (Writer w = resp.getWriter()) {
      w.write(String.format("%s = %d", sb, z));
    }
  }

}
