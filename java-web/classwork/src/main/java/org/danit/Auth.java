package org.danit;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public class Auth {

  public static String CookieName = "UID";
  private static final Cookie[] c0 = new Cookie[0];

  public static Optional<UUID> tryGetCookie(Cookie[] cookies) {
    return Arrays.stream(cookies != null ? cookies : c0)
        .filter(c -> c.getName().equals(CookieName))
        .findAny()
        .map(Cookie::getValue)
        .map(UUID::fromString);
  }

  public static Optional<UUID> tryGetCookie(HttpServletRequest rq) {
    Cookie[] cookies = rq.getCookies(); // NULL, not []
    return tryGetCookie(cookies);
  }

  public static UUID getCookieOrThrow(HttpServletRequest rq) {
    return tryGetCookie(rq).orElseThrow(() -> new RuntimeException("Cookie is absent"));
  }

}
