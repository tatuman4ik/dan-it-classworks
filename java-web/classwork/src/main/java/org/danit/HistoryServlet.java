package org.danit;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

@RequiredArgsConstructor
public class HistoryServlet extends HttpServlet {

  private final History history;

  @SneakyThrows
  private void redirect(HttpServletResponse resp, String to) {
    resp.sendRedirect(to);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    UUID uid = Auth.getCookieOrThrow(req);

    try (PrintWriter w = resp.getWriter()) {
      history.getAll(uid).forEach(w::println);
    }

  }

}
