package org.danit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class HtmlPageServlet extends HttpServlet {

  // http://localhost:8080/html
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String uri = getClass()
        .getClassLoader()
        .getResource("page.html")
        .getFile();
    Path fileWithFullPath = Paths.get(uri);

    try (PrintWriter w = resp.getWriter()) {
      Files
          .readAllLines(fileWithFullPath)
          .forEach(w::println);
    }
  }

}
