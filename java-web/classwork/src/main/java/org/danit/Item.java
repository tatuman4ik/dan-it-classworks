package org.danit;

import java.time.LocalDateTime;

public record Item(int x, int y, int z, LocalDateTime at) {

  public static Item of(int x, int y, int z) {
    return new Item(x, y, z, LocalDateTime.now());
  }

  @Override
  public String toString() {
    return String.format("%d + %d = %d, at = %s", x, y, z, at);
  }

}
