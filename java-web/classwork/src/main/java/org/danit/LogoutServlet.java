package org.danit;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

public class LogoutServlet extends HttpServlet {

  // TODO: GET -> show form
  // TODO: POST -> process form (logout user)

  // http://localhost:8080/logout
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    // creating
    Cookie c = new Cookie("UID", "");
    c.setMaxAge(0); // seconds

    // attach
    resp.addCookie(c);

    try (PrintWriter w = resp.getWriter()) {
      w.write("user logged out");
    }
  }
}
