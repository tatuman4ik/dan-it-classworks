package org.danit;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BinaryContentServlet extends HttpServlet {

  // http://localhost:8080/bin
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String uri = getClass()
        .getClassLoader()
        .getResource("Colors-HP-Logo.jpg")
        .getFile();
    System.out.println(uri);
    Path fileWithFullPath = Paths.get(uri);

    try (ServletOutputStream os = resp.getOutputStream()) {
      Files.copy(fileWithFullPath, os);
    }
  }

}
