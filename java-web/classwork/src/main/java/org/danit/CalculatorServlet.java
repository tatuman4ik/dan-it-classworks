package org.danit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.UUID;

public class CalculatorServlet extends HttpServlet {

  private final History history;

  public CalculatorServlet(History history) {
    this.history = history;
  }

  // http://localhost:8080/calc?x=5&y=7
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    UUID uid = Auth.getCookieOrThrow(req);

    String xs = req.getParameter("x");
    String ys = req.getParameter("y");
    String ks = req.getParameter("bla-bla"); // null

    int x = Integer.parseInt(xs);
    int y = Integer.parseInt(ys);
    int z = x + y;
    history.put(Item.of(x, y, z), uid);

    try (Writer w = resp.getWriter()) {
      w.write(String.format("%d + %d = %d", x, y, z));
    }

    if (true) resp.sendRedirect("http://localhost/login");

  }

}
