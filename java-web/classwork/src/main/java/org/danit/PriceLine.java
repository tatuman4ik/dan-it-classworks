package org.danit;

public class PriceLine {
  private final Integer id;
  private final String name;
  private final Double price;

  public PriceLine(Integer id, String name, Double price) {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getNameUpperCase() {
    return name.toUpperCase();
  }

  public Double getPrice() {
    return price;
  }
}
