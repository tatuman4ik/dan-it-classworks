package org.danit;

import lombok.SneakyThrows;
import sql.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class HistoryInDb implements History {

  private final Connection conn;

  public HistoryInDb(Connection conn) {
    this.conn = conn;
  }

  @SneakyThrows
  private void saveToDb(Item item, UUID id) {
    String sql1 = "INSERT INTO history (x, y, z) values (?,?,?)";
    String sql2 = "INSERT INTO history (x, y, z, t) values (?,?,?,?)";

    PreparedStatement st = conn.prepareStatement(sql1);
    st.setInt(1, item.x());
    st.setInt(2, item.y());
    st.setInt(3, item.z());
//    st.setObject(4, item.at());
    st.execute();
  }

  @Override
  public void put(Item item, UUID id) {
    saveToDb(item, id);
  }

  public Iterable<Item> getAllFromDb() throws Exception {
    PreparedStatement st = conn.prepareStatement(
        """
            SELECT *
            FROM history
            ORDER BY id
            """
    );
    ResultSet rs0 = st.executeQuery();
    return DbUtils.convert(rs0, rs -> {
      int x = rs.getInt("x");
      int y = rs.getInt("y");
      int z = rs.getInt("z");
//      Timestamp t = rs.getTimestamp("t");
//      Instant instant = t.toInstant();
//      LocalDateTime at = LocalDateTime.from(instant);
//      Item item = new Item(x, y, z, at);
      return Item.of(x, y, z);
    });
  }

  @Override
  public Iterable<Item> getAll(UUID id) {
    try {
      return getAllFromDb();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
