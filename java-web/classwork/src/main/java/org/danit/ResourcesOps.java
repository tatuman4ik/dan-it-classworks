package org.danit;

import java.net.URISyntaxException;

public class ResourcesOps {

  public static String dirUnsafe(String dir) {
    try {
      return ResourcesOps.class
          .getClassLoader()
          .getResource(dir)
          .toURI()
          .getPath();
    } catch (URISyntaxException e) {
      throw new RuntimeException(String.format("Requested path `%s`not found", dir), e);
    }
  }

}
