package org.danit;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

public class LoginServlet extends HttpServlet {

  // TODO: GET -> show form
  // TODO: POST -> process form (login user)


  // http://localhost:8080/login
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    // create
    Cookie c = new Cookie("UID", UUID.randomUUID().toString());
//    c.setDomain("..."); // ???
//    c.setPath("..."); // ???
    // attach
    resp.addCookie(c);

//    Cookie c2 = new Cookie("XID", UUID.randomUUID().toString());
//    c2.setMaxAge(60 * 60); // seconds (1 hour)
//    resp.addCookie(c2);

    try (PrintWriter w = resp.getWriter()) {
      w.write("user registered");
    }
  }
}
