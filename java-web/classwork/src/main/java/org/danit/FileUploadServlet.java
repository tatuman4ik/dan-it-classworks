package org.danit;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;

public class FileUploadServlet extends HttpServlet {

  // http://localhost:8080/upload
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
    cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
    cfg.setDirectoryForTemplateLoading(new File(ResourcesOps.dirUnsafe("tpl")));

    try (PrintWriter w = resp.getWriter()) {
      cfg
          .getTemplate("upload.ftl")
          .process(new HashMap<String, Object>(), w);
    } catch (TemplateException x) {
      throw new RuntimeException(x);
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    try (PrintWriter w = resp.getWriter()) {
      for (Part part : req.getParts()) {
        System.out.println(part.getSubmittedFileName());
        try (InputStream is = part.getInputStream()) {
          byte[] bytes = is.readAllBytes();
//        FileOutputStream os = new FileOutputStream("/tmp/"+ part.getSubmittedFileName());
//        is.transferTo(os);
//        os.close();
          String s = new String(bytes);
          w.println(s);
          w.println("\n");
        }
      }
    }
  }
}
