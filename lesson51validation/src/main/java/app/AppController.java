package app;

import app.model.PersonForm;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AppController {

    // http://localhost:8080/
    @GetMapping
    public String formShow(PersonForm personForm) {
        return "form";
    }

    @PostMapping
    public String formValidate(@Valid PersonForm personForm, BindingResult result) {
        System.out.println(personForm);
        if (result.hasErrors()) {
            List<FieldError> fieldErrors = result.getFieldErrors();
            fieldErrors.forEach(System.out::println);
            return "form";
        }

        return "redirect:/ok";
    }

    @GetMapping("ok")
    public String  handleOk() {
        return "ok";
    }

}
