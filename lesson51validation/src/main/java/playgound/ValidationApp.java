package playgound;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

public class ValidationApp {

    public static <A> Validated validateV2(A x, Function<A, Validated>... functions) {
        return Arrays.stream(functions)
                .map(f -> f.apply(x))
                .filter(r -> r instanceof Validated.Invalid)
                .findFirst()
                .orElse(Validated.valid(x));
    }

    public static Validated nonNull(Object x) {
        return x == null ?
                Validated.invalid("shouldn't be bull") :
                Validated.valid(x);
    }

    public static Validated nonNegative(Integer x) {
        return x < 0 ?
                Validated.invalid("should be non negative") :
                Validated.valid(x);
    }

    public static Validated greaterThan(Integer x, Integer n) {
        return (x <= n) ?
                Validated.invalid(String.format("should be greater than %d", n)) :
                Validated.valid(x);
    }
    /* age validation:
    * x >= 0
    * x < 100
    * ...
    */
    public static Validated validateV2(Integer x) {
        return validateV2(
                x,
                ValidationApp::nonNull,
                ValidationApp::nonNegative,
                a -> greaterThan(x, a)
                );
    }

    public static Validated validateV5(Integer x) {
        if (x == null) return Validated.invalid("age shouldn't be bull");
        if (x < 0) return Validated.invalid("age should be non negative");
        if (x > 100) return Validated.invalid("age should be less than 100");
        return Validated.valid(x);
    }

    /* check for number */
    public static Validated checkForNumber(String x) {
        if (x == null) return Validated.invalid("value shouldn't be bull");
        if (x.isEmpty()) return Validated.invalid("value shouldn't be empty");
        try {
            Integer.parseInt(x);
            return Validated.valid(x);
        } catch (Exception ex) {
            return Validated.invalid("non string given");
        }
    }

    public static Validated nonNegativeV1(Integer x) {
        if (x == null) return Validated.invalid("age shouldn't be bull");
        if (x < 0) return Validated.invalid("age shouldn't be negative");
        return Validated.valid(x);
    }

    public static Validated isAdult(Integer x) {
        if (x == null) return Validated.invalid("age shouldn't be bull");
        if (!(x >= 18)) return Validated.invalid("age shouldn't be gt 18");
        return Validated.valid(x);
    }

    class LoginForm {
        String login;
        String password;
    }

    static Validated<String> validateLogin(String login) {
        throw new IllegalArgumentException("I don't care about implementation");
    }

    static Validated<String> validatePassword(String password) {
        throw new IllegalArgumentException("I don't care about implementation");
    }

    static Validated<LoginForm> validateForm(LoginForm form) {
        Validated<String> loginValidated = validateLogin(form.login);
        Validated<String> passwordValidated = validatePassword(form.password);

        if (loginValidated.isValid() && passwordValidated.isValid()) return Validated.valid(form);
        ArrayList<String> errors = new ArrayList<>();
        if (!loginValidated.isValid()) errors.addAll(loginValidated.errors());
        if (!passwordValidated.isValid()) errors.addAll(passwordValidated.errors());
        return Validated.invalidList(errors);
    }


}
