package app.controller;

import app.service.Service003;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AppController {

  private final Service003 service003;

  @GetMapping("p")
  public ResponseEntity<?> handle() {
    return service003.obtain();
  }

}
