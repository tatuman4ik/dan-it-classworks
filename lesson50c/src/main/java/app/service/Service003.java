package app.service;

import app.models.Person;
import app.models.PersonDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Log4j2
@PropertySource("classpath:api-secret.properties")
public class Service003 {

  @Value("${api.code}")
  String code;

  private final RestTemplate rest;

  public Person obtain0() {
    String addr = "http://localhost:8080/person?id=2";
    ResponseEntity<Person> response = rest.getForEntity(addr, Person.class);
    HttpStatus code = response.getStatusCode();
    log.info(code);

    Person p = response.getBody();
    log.info(p);

    Person p2 = new Person(p.id() * 10, p.name().toUpperCase());
    log.info(p2);

    return p2;
  }

  public Person obtain1() {
    String addr = "http://localhost:8080/person?id=2";

    RequestEntity<Void> request = RequestEntity
        .method(HttpMethod.GET, URI.create(addr))
        .header(HttpHeaders.AUTHORIZATION, code)

        .build();

    ResponseEntity<Person> response = rest.exchange(request, Person.class);
    HttpStatus code = response.getStatusCode();
    log.info(code);

    Person p = response.getBody();
    log.info(p);

    Person p2 = new Person(p.id() * 10, p.name().toUpperCase());
    log.info(p2);

    return p2;
  }

  public ResponseEntity<?> obtain() {
    String addr = "http://localhost:8080/person?id=2";
    Person p = rest.getForObject(addr, Person.class);
    log.info(p);
    Person p2 = new Person(p.id() * 10, p.name().toUpperCase());
    log.info(p2);
    return ResponseEntity.ok(p2);
  }

  private ResponseEntity<?> makeResponse(Person p) {
    log.info(p);
    Person p2 = new Person(p.id() * 10, p.name().toUpperCase());
    log.info(p2);
    return ResponseEntity.ok(p2);
  }

  public ResponseEntity<?> obtain4() {
    String addr = "http://localhost:8080/person";

    RequestEntity<PersonDetails> request = RequestEntity
        .method(HttpMethod.POST, URI.create(addr))
        .header(HttpHeaders.AUTHORIZATION, code)
        .body(new PersonDetails(7, "James Bond"));

    ResponseEntity<Person> response = rest.exchange(request, Person.class);
    HttpStatus code = response.getStatusCode();
    log.info(code);

    return switch (code) {
      case OK -> makeResponse(response.getBody());
//      case BAD_REQUEST -> ;
//      case FORBIDDEN   -> ;
//      case CONFLICT    -> ;
      default -> ResponseEntity.status(code).build();
    };
  }

}
