package app;

import org.springframework.stereotype.Service;

@Service
public class FormatterLower implements Formatter{
  @Override
  public String format(String line) {
    return line.toLowerCase();
  }
}
