package app.data;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class TableItem {
  String language;
  Integer version;
}
