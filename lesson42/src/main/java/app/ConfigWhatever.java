package app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class ConfigWhatever {

  @Primary
  @Bean
  public Formatter formatterBean() {
    return new FormatterUpper();
  }

}
