package app;

import app.forms.LoginForm;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@Log4j2
public class FormController {

  @GetMapping("form")
  public String formShow() {
    return "form";
  }

  @PostMapping("form")
  @ResponseBody
  public String formProcess(LoginForm form, HttpServletRequest rq) {
    Map<String, String[]> allParams = rq.getParameterMap();
    log.info("LOGIN: {}", form.getLogin());
    log.info("PASWD: {}", form.getPassword());
    return "OK";
  }

}
