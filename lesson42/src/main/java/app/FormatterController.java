package app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("fmt")
public class FormatterController {

  private final Formatter formatter;

  public FormatterController(
//      @Qualifier("formatterUpper")
      Formatter formatter) {
    this.formatter = formatter;
  }

  @GetMapping("{path}")
  public String fmt(@PathVariable("path") String path) {
    return formatter.format(path);
  }

}

