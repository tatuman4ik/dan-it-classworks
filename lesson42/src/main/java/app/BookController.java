package app;

import app.data.TableHeader;
import app.data.TableItem;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
@RequestMapping("books")
public class BookController {

  private final AtomicInteger counter  = new AtomicInteger();

  @GetMapping("java-book1")
  public String javaBook1() {
    // counter = 10
    int a = counter.incrementAndGet(); // 550
    int b = counter.getAndIncrement(); // 10..549
    // counter = 550
    return "java";
  }

  private Map<String, Object> data() {
    TableHeader th = new TableHeader("Language", "version");
    List<TableItem> tb = List.of(
        new TableItem("Java", 8),
        new TableItem("Java", 11),
        new TableItem("Java", 17),
        new TableItem("Scala", 2),
        new TableItem("Scala", 3)
    );
    return Map.of(
        "thead", th,
        "tbody", tb
    );
  }

  @GetMapping("java-books1")
  public String javaBooks1(Model model) {
    data().forEach((k, v) -> model.addAttribute(k, v));
    return "java-books";
  }

  @GetMapping("java-books2")
  public ModelAndView javaBooks2() {
    return new ModelAndView("java-books", data());
  }


  @GetMapping("java-book3")
  public String javaBook3() {
    return "redirect:scala-book";
  }

  @GetMapping("java-book4")
  public RedirectView javaBook4() {
    return new RedirectView("scala-book");
  }

  @GetMapping("scala-book")
  public String scalaBook() {
    return "scala";
  }

}
