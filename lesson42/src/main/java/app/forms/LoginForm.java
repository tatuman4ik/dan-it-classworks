package app.forms;

import lombok.Data;

@Data
public class LoginForm {
  String login;
  String password;
}
