package playground;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

public class HashPlayground {

  public static void main(String[] args) {
    PasswordEncoder enc = new Pbkdf2PasswordEncoder();
//    System.out.println(enc.encode("123"));
//    System.out.println(enc.encode("123"));
//    System.out.println(enc.encode("123"));
//    System.out.println(enc.encode("123"));
    String hashed1 = "{pbkdf}3fbfd5470e8f8ab2ddf3702c72828621a9ad54e74b0cab2332ed87ec9a62476ce5c6398d33667b27";
    String hashed2 = "{pbkdf}5c590ae0a8786c21168f7db10b61211c64a0764498d5107cc3353567bf51786bdb7ab5def8832598";
    String hashed3 = "{pbkdf}420e159e01659e3f3b7d3eb0ee6cc6a2a129d7ff87bc0640655e2c6067d53563c70b4215f954adfa";
    String hashed4 = "{pbkdf}950c1609cd50d01899b97ada010d76c9c83bf22cd808840b61f798fa2f396cda77fd96ab4d720428";

    System.out.println(enc.matches("123", hashed1)); // true
    System.out.println(enc.matches("123", hashed2)); // true
    System.out.println(enc.matches("123", hashed3)); // true
    System.out.println(enc.matches("123", hashed4)); // true
    System.out.println(enc.matches("123a", hashed1)); // false

  }

}
