package app.ex;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Arrays;

@ControllerAdvice
// all handlers
// to ALL CONTROLLERS
public class ApplicationWideErrorHandling {

  @ExceptionHandler({WrongBookException.class})
  public ResponseEntity<ErrorMessage> handle(WrongBookException x) {
    return ResponseEntity.status(501).body(
        ErrorMessage.of(
            String.format(
                "Wrong book id given: %d, Learn modern Java!", x.wrongBookId
            )
        )
    );
  }

  @ExceptionHandler({OtherException.class})
  public ResponseEntity<ErrorMessage> handle(OtherException ex) {
    return ResponseEntity.status(502).body(
        new ErrorMessage("Other...",
            Arrays.stream(ex.getStackTrace())
                .takeWhile(st -> !st.getClassName().startsWith("jdk."))
                .toArray(StackTraceElement[]::new)
        )
    );
  }

}
