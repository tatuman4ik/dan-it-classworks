package app.ex;

public class WrongBookException extends AppException {

  public final Integer wrongBookId;

  public WrongBookException(Integer wrongBookId) {
    super();
    this.wrongBookId = wrongBookId;
  }

}
