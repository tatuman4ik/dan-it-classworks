package app.ex;

public record ErrorMessage(String message, StackTraceElement[] stack) {

  public static ErrorMessage of(String message) {
    return new ErrorMessage(message, new StackTraceElement[0]);
  }

}
