package app.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

//@Controller
public class IAmLazy implements ErrorController {

  @GetMapping("/error")
  @ResponseBody
  public String simple() {
    return "I'm lazy I don't care. Something went wrong";
  }
}
