package app.controller;

import app.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("author")
@RequiredArgsConstructor
public class AuthorController {

  private final AuthorService service;

  @GetMapping
  public ResponseEntity<?> handle() {
    return ResponseEntity.ok().body(service.get1());
  }

}
