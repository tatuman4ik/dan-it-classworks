package app.controller;

import app.ex.OtherException;
import app.service.BookService;
import app.entity.Book;
import app.ex.ErrorMessage;
import app.ex.WrongBookException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("book")
@RequiredArgsConstructor
public class BookController {

  private final BookService service;

  @GetMapping("{id}")
  public Book handle1(@PathVariable("id") Integer id) {
    return service.get1(id);
  }

  @GetMapping("notmine")
  public Book handle2() {
    return service.get2();
  }


}
