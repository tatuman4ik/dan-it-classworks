package app.service;

import app.entity.Book;
import app.ex.OtherException;
import app.ex.WrongBookException;
import org.springframework.stereotype.Service;

@Service
public class BookService {

  public Book get1(Integer id) {
    if (id < 8) throw new WrongBookException(id);
    return new Book(id, "Java");
  }

  public Book get2() {
    int x;

    try {
      x = 1 / 0;
    } catch (ArithmeticException aex) {
      throw new OtherException();
    }

    return new Book(8, "Java");
  }


}
