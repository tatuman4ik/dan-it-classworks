package playground;

public class InfinityException {

  public static void main(String[] args) {
    double x = 10.0 / 0.0; // Infinity
    double y = 5.0 / 0.0;  // Infinity
    double a = x / y;      // Nan
    double b = x - y;      // Nan
    System.out.println(x);
    System.out.println(y);
    System.out.println(a);
    System.out.println(b);
  }

}
