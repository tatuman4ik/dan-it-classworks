package playground;

import java.util.function.Function;

public class EitherPlayground {

  // String => Int | Double
  public Either<Integer, Double> convert(String x) {
    throw new RuntimeException();
  }

  // Function<Integer, Boolean>
  public Boolean f1(Integer x) {
    throw new RuntimeException();
  }

  // Function<Double, Boolean>
  public Boolean f2(Double x) {
    throw new RuntimeException();
  }

  public Function<String, Boolean> composition() {
    return new Function<String, Boolean>() {
      @Override
      public Boolean apply(String s) {
        Either<Integer, Double> converted = convert(s);
        if (converted instanceof Either.Left) {
          Integer l = ((Either.Left<Integer, Double>) converted).l;
          return f1(l);
        }
        if (converted instanceof Either.Right) {
          Double r = ((Either.Right<Integer, Double>) converted).r;
          return f2(r);
        }
        throw new IllegalArgumentException("never happens");
      }
    };
  }

}
