package playground;

import java.util.function.BiFunction;
import java.util.function.Function;

public class PlaygroundException {

  // (Int, Int) => Int | Exception
  public static int divUnsafe(int a, int b) {
    return a / b;
  }

  public static BiFunction<Integer, Integer, Integer> makeSafe1(
      BiFunction<Integer, Integer, Integer> unsafeFn,
      Function<Exception, Integer> handler) {
    return (a, b) -> {
      try {
        return unsafeFn.apply(a, b);
      } catch (Exception x) {
        return handler.apply(x);
      }
    };
  }

  public static <A, B, C, D> BiFunction<A, B, D> makeSafe2(
      BiFunction<A, B, C> unsafeFn,
      Function<C, D> resultRemapper,
      Function<Exception, D> handler) {
    return (a, b) -> {
      try {
        C result = unsafeFn.apply(a, b);
        D remapped = resultRemapper.apply(result);
        return remapped;
      } catch (Exception x) {
        return handler.apply(x);
      }
    };
  }

  public static void main(String[] args) {
    BiFunction<Integer, Integer, Integer> safe1 = makeSafe1(
        PlaygroundException::divUnsafe,
        x -> 9999
    );

    BiFunction<Integer, Integer, String> safe2 = makeSafe2(
        PlaygroundException::divUnsafe,
        a -> String.format("Result is OK: %d", a),
        x -> String.format("Exception handled: %s", x.getMessage())
    );

    System.out.println(safe1.apply(10, 0));

    System.out.println(safe2.apply(10, 2));
    System.out.println(safe2.apply(10, 0));

  }

}
