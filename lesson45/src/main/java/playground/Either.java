package playground;

public interface Either<L, R> {

  class Left<L, R> implements Either<L, R>{

    public final L l;

    public Left(L l) {
      this.l = l;
    }
  }

  class Right<L, R> implements Either<L, R>{
    public final R r;

    public Right(R r) {
      this.r = r;
    }

  }

}
