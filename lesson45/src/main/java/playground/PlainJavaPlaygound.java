package playground;

public class PlainJavaPlaygound {


  public static String convert(String raw) {
    try {
      Integer i = Integer.parseInt(raw);
      return String.format("%s converted successfully", raw);
    } catch (NumberFormatException x) {
      return String.format("%s convert failure with error: %s", raw, x.getMessage());
    }
  }


  public static void main(String[] args) {
    String result = convert("123x");
    System.out.println(result);
  }

}
