package app;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;

@SpringBootApplication
public class ConsumerLauncher {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerLauncher.class, args);
    }

    @Bean
    public NewTopic topic() {
        return TopicBuilder.name("orders")
            .partitions(1)
            .replicas(1)
            .build();
    }
}
