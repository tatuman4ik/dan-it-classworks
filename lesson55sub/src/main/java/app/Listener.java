package app;

import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class Listener {

  public Listener() {
    System.out.println(">>> creating listener");
  }

  @KafkaListener(id = "group1", topics = "orders")
  public void onNewMessage(ConsumerRecord<String, String> cr) {
    System.out.println("got message");
    System.out.println(cr);
    String key = cr.key();
    String value = cr.value();
    long offset = cr.offset();
    long timestamp = cr.timestamp();
    log.info(
        String.format(
            "TS:%s, TOPIC:%s, KEY:%s, VALUE, %S",
            timestamp,
            offset,
            key,
            value
        )
    );
  }


}
