package app.repo;

import app.models.AnyCombination;
import app.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

// https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
public interface PersonRepo extends JpaRepository<Person, Integer> {

  // select distinct * from person where firstname like s1 and lastname like s2
  List<Person> getDistinctAllByFirstNameContainsAndLastNameContains(String s1, String s2);

//  @Query(".................................")
//  List<AnyCombination> getMyCreature();

}
