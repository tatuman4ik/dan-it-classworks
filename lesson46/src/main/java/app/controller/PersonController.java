package app.controller;

import app.dto.PersonToCreate;
import app.dto.PersonToShow;
import app.models.Person;
import app.repo.PersonRepo;
import app.service.PersonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.List;

@Log4j2
@RestController
@RequiredArgsConstructor
public class PersonController {

  private final EntityManager em;
  private final PersonService personService;

  ////////////////////////////////////////////////////

  @PostMapping("c1")
  public void handlePost1(@RequestBody Person p) {
    log.info(p);
    personService.create1(p);
  }

  @PostMapping("c2")
  public void handlePost1(@RequestBody PersonToCreate p) {
    personService.create(p);
  }

  ////////////////////////////////////////////////////

  @GetMapping
  public List<PersonToShow> handleGet() {
    return personService.findAll();
  }

  ////////////////////////////////////////////////////


  @GetMapping("em")
  public Person handleGet2() {
    Person person = em.find(Person.class, 2);
    return person;
  }

}
