package app.service;

import app.dto.PersonToCreate;
import app.dto.PersonToShow;
import app.models.Person;
import app.repo.PersonRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PersonService {

  private final PersonRepo personRepo;

  public List<PersonToShow> findAll() {
    return personRepo.findAll()
        .stream()
        .map(p -> {
          PersonToShow pp = new PersonToShow() {{
            setId(p.getId());
                Optional.ofNullable(p.getFirstName())
                    .map(fn -> fn + Optional.ofNullable(p.getLastName()).orElse(""))
                    .ifPresent(this::setName);
            setGroup(p.getGroup());
          }};
          return pp;
        })
        .filter(p -> p.getName() != null)
        .filter(p -> !p.getName().isBlank())
        .toList();
  }

  public void create(PersonToCreate p) {
    Person person = new Person();
    person.setFirstName(p.getName());
    personRepo.save(person);
  }

  public void create1(Person p) {
    personRepo.save(p);
  }

}
