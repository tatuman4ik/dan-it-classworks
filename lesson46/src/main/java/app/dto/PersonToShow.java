package app.dto;

import lombok.Data;

@Data
public class PersonToShow {
  private Integer id;
  private String name;
  private String group;
}
