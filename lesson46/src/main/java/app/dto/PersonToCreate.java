package app.dto;

import lombok.Data;

@Data
public class PersonToCreate {
  private String name;
}
