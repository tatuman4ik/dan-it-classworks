package app;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProperties {

  public static final String id = "UserProperties";

  String language;
  Integer resolution;

  static UserProperties initial() {
    return new UserProperties("UA", 3840);
  }

}
