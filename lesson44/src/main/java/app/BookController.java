package app;

import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("book")
public class BookController {

  private final static String cookieName = "JID";
  private final static String atNameLang = "language";
  private final static String atNameRes = "resolution";
  private final ExecutorService esIO = Executors.newFixedThreadPool(10);

  // 1. set a cookie
  @SneakyThrows
  @GetMapping("a")
  public String handle_a(HttpServletResponse rs) {

    rs.addCookie(
        new Cookie(cookieName, UUID.randomUUID().toString())
    );

    CompletableFuture<String> result =
        CompletableFuture.completedFuture("cookie has been set");

    result.thenApplyAsync(x -> {
              System.out.println(x);
              return x;
            }, esIO);

    return result.get();
  }

  // 2. remove a cookie
  @GetMapping("b")
  public String handle_b(HttpServletResponse rs) {
    Cookie c = new Cookie(cookieName, "");
    c.setMaxAge(0);
    rs.addCookie(c);

    return "cookie has been removed";
  }

  @GetMapping("c")
  public String handle_c(HttpServletRequest rq) {
    String x = Optional.ofNullable(rq.getCookies())
        .flatMap(cc -> Arrays.stream(cc)
            .filter(c -> c.getName().equals(cookieName))
            .findFirst()
        )
        .map(c -> c.getValue())
        .orElse("< no cookie >");

    return String.format("cookie has been accessed %s", x);
  }

  @GetMapping("d")
  public String handle_d(@CookieValue(cookieName) Cookie x) {
    return String.format("cookie has been accessed %s", x.getValue());
  }

  @GetMapping("e")
  public String handle_e(@CookieValue(cookieName) Optional<Cookie> x) {
    return String.format("cookie has been accessed %s", x.map(Cookie::getValue));
  }

  @GetMapping("s1")
  public String handle_s1(HttpServletRequest rq) {
    HttpSession session = rq.getSession();
    String id = session.getId();
    return String.format("Session obtained %s", id);
  }

  @GetMapping("s2")
  public String handle_s2(HttpSession session) {
    String id = session.getId();
    return String.format("Session obtained %s", id);
  }

  @GetMapping("s3/{lang}/{res}")
  public String handle_s3(HttpSession session,
                          @PathVariable("lang") String lang,
                          @PathVariable("res") Integer res
                          ) {
    session.setAttribute(atNameLang, lang);
    session.setAttribute(atNameRes, res);
    return String.format("Attributes set");
  }

  @GetMapping("s4")
  public String handle_s4(HttpSession session) {
    String lang = (String) session.getAttribute(atNameLang);
    Integer res = (Integer) session.getAttribute(atNameRes);
    return String.format("Attributes accessed: lang: %s, res: %d", lang, res);
  }

  @GetMapping("s5")
  public String handle_s5(HttpSession session) {
    session.invalidate();
    return String.format("session invalidated");
  }





}
