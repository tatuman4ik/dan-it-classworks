package app;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("book2")
@SessionAttributes(UserProperties.id)
//@SessionAttributes(types = {UserProperties.class})
public class Book2Controller {

  @ModelAttribute(UserProperties.id)
  public UserProperties initial() {
    return UserProperties.initial();
  }

  // 1. set attributes
  @GetMapping("a1/{lang}/{res}")
  public String handle_a1(HttpSession session,
                          @ModelAttribute(UserProperties.id) UserProperties properties,
                          @PathVariable("lang") String lang,
                          @PathVariable("res") Integer res
                          ) {
    properties.language = lang;
    properties.resolution = res;
    return String.format("Attributes set");
  }

  // 2. access attributes
  @GetMapping("a2")
  public String handle_a2(HttpSession session,
                          @ModelAttribute(UserProperties.id) UserProperties properties) {
    return String.format("Attributes accessed: lang: %s, res: %d", properties.language, properties.resolution);
  }

}
