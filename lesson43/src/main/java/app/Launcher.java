package app;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Map;

@SpringBootApplication
public class Launcher {

  public static void main(String[] args) {
    SpringApplication.run(Launcher.class, args);
  }

  @Bean
  public CommandLineRunner runit(ApplicationContext ctx) {
    return args -> {
      MyCalculator calc = ctx.getBean(MyCalculator.class);
      System.out.println(calc.add(1,2));
//      Map<String, Object> beansOfType = ctx.getBeansOfType(Object.class);
//      beansOfType.forEach((k, v) -> System.out.println(k));
    };
  }



}
