package app;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("files")
public class DownloadController {

  @GetMapping("{filename}")
  public ResponseEntity<Resource> get(@PathVariable String filename) {
    Resource file = new ClassPathResource(filename);
    return ResponseEntity.ok()
        .header(
            HttpHeaders.CONTENT_DISPOSITION,
            "attachment; filename=\"data.txt\""
//            String.format("attachment; filename=\"data.txt\"", file.getFilename())
        )
        .body(file);
  }

}
