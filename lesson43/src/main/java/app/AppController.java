package app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("app")
public class AppController {

  int x=0;

  @GetMapping("page1")
  public String page1() {
    x++;
    return "page1";
  }

  @GetMapping("page2")
  public String page2() {
    System.out.println(x);
    return "page2";
  }

}
