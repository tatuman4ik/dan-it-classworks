package app;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Log4j2
@Controller
public class UploadController {

  @GetMapping("upload")
  public String handle_form_get() {
    return "upload-form";
  }

  @PostMapping("upload")
  @ResponseBody
  //@ResponseStatus(HttpStatus.OK)
  public ResponseEntity<?> handle_form_post(@RequestParam("filea") MultipartFile file) throws IOException {
    String s = new String(file.getBytes());
    log.info("GOT: {}", s);
    return ResponseEntity.status(200).build();
  }

}
