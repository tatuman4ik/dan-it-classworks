package app;

import org.springframework.stereotype.Service;

@Service
public class MyCalculator {
  public int add(int x, int y) {
    return x + y;
  }
}
