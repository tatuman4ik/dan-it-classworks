package app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("post")
@Log4j2
@RequiredArgsConstructor
public class PostController {

  private final ObjectMapper om;

  @PostMapping
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void test(@RequestBody Person p) {
    log.info(p);
  }

  @GetMapping("1")
  public void test2() throws JsonProcessingException {
    Person p = new Person(11, "Alex");
    String serialized = om.writeValueAsString(p);
    System.out.println(serialized);
  }


}
