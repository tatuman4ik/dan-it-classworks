package a2binary;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static tools.EX.NI;

public class IntToBin {

    static String intToBin1(int x) {
        StringBuilder bin = new StringBuilder();
        while (x > 0) {
            bin.append(
                    x & 1
//                  x % 2
            );
//          x = x / 2;
            x = x >> 1;
        }
        String data = bin.reverse().toString();
        return "0".repeat(8 - data.length()) + data;
    }

    public static String intToBin2(int x) {
        StringBuilder data = new StringBuilder();
        while (x > 0) {
            data.append(x % 2 == 0 ? "0" : "1");
            x /= 2;
        }
        return "0".repeat(8 - data.length()) + data;
    }

    public static String intToBin3(int x) {
        StringBuilder sb = new StringBuilder();
        long y;
        if (x < 0) {
            sb.append(1);
            y = -1 * x;
        } else {
            sb.append(0);
            y = x;
        }
        for (int i = 7; i > 0; i--) {
            if (y / Math.pow(2, i) > 0) {
                sb.append(1);
            } else sb.append(0);
            y = (long) (y - y / Math.pow(2, i));
        }
        return sb.toString();
    }

    public static String intToBin4(int x) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < 256; i = i << 1) {
            String digit = (i & x) == i ? "1" : "0";
            sb.append(digit);
        }
        return sb.reverse().toString();
    }

    public static String intToBin5(int x) {
        StringBuilder sb = new StringBuilder();
        for (int i = 7; i >= 0; i--) {
            int mask = 1 << i;
            int value = x & mask;
            sb.append(value > 0 ? "1" : "0");
        }
        return sb.toString();
    }

    public static String intToBin6(int x) {
        StringBuilder sb = new StringBuilder();
        for (int i = 7; i >= 0; i--) {
            int bit = (x >> i) & 0b00000001;
            sb.append(
                    bit
//                  bit > 0 ? "1" : "0"
            );
        }
        return sb.toString();
    }

    public static String intToBin7(int x) {
        byte[] bytes = new byte[8];
        for (int i = 7; i >= 0; i--) {
            int bit = (x >> i) & 0b00000001;
            bytes[7 - i] = (byte) ('0' + bit);// > 0 ? (byte)'1' : (byte)'0';
//            bytes[7 - i] = bit > 0 ? (byte)'1' : (byte)'0';
        }
        return new String(bytes);
    }

    public static String intToBin(int x) {
        return IntStream.rangeClosed(0, 7)     // 0..7
                .map(i -> 7 - i)               // 7..0
                .map(i -> (x >> i) & 1)        // 0 / 1
                .mapToObj(Integer::toString)   // "0" / "1"
                .collect(Collectors.joining());
    }

    // 0..255
    public static void main(String[] args) {
        System.out.println(intToBin(1));   // 00000001
        System.out.println(intToBin(16));  // 00010000
        System.out.println(intToBin(128)); // 10000000
        System.out.println(intToBin(234)); // 11101010
        System.out.println(intToBin(255)); // 11111111
    }

}
