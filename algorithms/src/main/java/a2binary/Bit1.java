package a2binary;

public class Bit1 {

    public static void main(String[] args) {
        System.out.println(4 | 1);  // 5
        System.out.println(12 | 2); // 14
        System.out.println(12 | 6); // 14
        System.out.println(4 & 1);  // 0
    }


}
