package a2binary;

public class XORApp {

    static String encode(String s, int code) {
        char[] chars = new char[s.length()];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char)(s.charAt(i) ^ code);
        }
        return new String(chars);
    }

    public static void main0(String[] args) {
        String s = "Hello";
        String s2 = encode(s, 33);
        System.out.println(s2); // iDMMN
        String s3 = encode(s2, 33);
        System.out.println(s3); // Hello
    }

    public static void main(String[] args) {
        String s0 = "Hello";

        String s1 = encode(s0, 10);
        String s2 = encode(s1, 33);
        String s3 = encode(s2, 17);

        String s7 = encode(s3, 58);

//        String s4 = encode(s3, 33);
//        String s5 = encode(s4, 17);
//        String s6 = encode(s5, 5);
//        String s7 = encode(s6, 15);

//        String s6 = encode(s5, 2);
//        String s7 = encode(s6, 8);
        System.out.println(s7);
    }




}
