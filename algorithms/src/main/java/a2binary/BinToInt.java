package a2binary;

import java.util.stream.IntStream;

public class BinToInt {

    static int intToBin1(String raw) {
        int outcome = 0;
        for (int i = 0; i < raw.length(); i++) {
            if (raw.charAt(i) == '1') {
                outcome += 1 << (raw.length() - i - 1);
            }
        }
        return outcome;
    }

    static int intToBin2(String raw) {
        int outcome = 0;
        for (int i = 0; i < raw.length(); i++) {
            outcome +=
                    (raw.charAt(i) - '0') // 0..1
                            * (1 << (raw.length() - i - 1));
            // 2^...
        }
        return outcome;
    }

    static int intToBin3(String raw) {
        int outcome = 0;
        for (int i = raw.length() - 1; i >= 0; i--) {
            outcome += (raw.charAt(i) - '0') * (1 << (7 - i));
        }
        return outcome;
    }

    static int intToBin(String raw) {
        return IntStream.range(0, raw.length())
                .map(i -> (raw.charAt(i) - '0') * (1 << (raw.length() - i - 1)))
                .sum();
    }

    // 0..255
    public static void main(String[] args) {
        System.out.println(intToBin("00000001")); //   1
        System.out.println(intToBin("00010000")); //  16
        System.out.println(intToBin("10000000")); // 128
        System.out.println(intToBin("11101010")); // 234
        System.out.println(intToBin("11111111")); // 255
    }

}
