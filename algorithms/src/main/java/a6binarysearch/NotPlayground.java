package a6binarysearch;

import java.util.stream.Stream;

public class NotPlayground {

    static String toBin(int x) {
        String s = Integer.toBinaryString(x);
        return "0".repeat(32 - s.length()) + s;
    }

    static String withSigh(int x) {
        return x > 0 ?
                String.format("+%010d", x) :
                String.format("%011d", x);
    }

    static String rep(int x) {
        return String.format("%s %s", toBin(x), withSigh(x));
    }

    static void p(int x) {
        System.out.println(rep(x));
    }

    public static void main(String[] args) {
        Stream.of(
                0,
                1,
                2,
                3,
                Integer.MAX_VALUE - 2,
                Integer.MAX_VALUE - 1,
                Integer.MAX_VALUE,
                Integer.MAX_VALUE + 1,
                Integer.MAX_VALUE + 2,
                Integer.MAX_VALUE + 3,
                Integer.MAX_VALUE + 4,
                Integer.MAX_VALUE + 5,
                -3,
                -4,
                -3,
                -2,
                -1
        ).forEach(NotPlayground::p);
    }

}
