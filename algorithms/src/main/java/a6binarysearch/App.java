package a6binarysearch;

import java.util.*;
import java.util.stream.Collectors;

public class App {

    static boolean search(int[] data, int needle) {
        int l = 0;
        int r = data.length - 1;
        while (l <= r) {
            int m = (l + r) >> 1;
//            int m = l + (r - l) / 2;
            if      (needle < data[m]) r = m - 1;
            else if (needle > data[m]) l = m + 1;
            else /* needle == data[m] */ return true;
        }
        return false;
    }

    static boolean searchWithCheckBefore(int[] data, int needle) {
        if (data.length == 0 || needle < data[0] || needle > data[data.length-1]) return false;
        return search(data, needle);
    }

    static boolean searchR(int[] data, int needle, int l, int r) {
        if (!(l <= r)) return false;
        int m = (l + r) >> 1;
        if      (needle < data[m]) return searchR(data, needle, l, m - 1);
        else if (needle > data[m]) return searchR(data, needle, m + 1, r);
        return true;
    }

    static boolean searchR(int[] data, int needle) {
        return searchR(data, needle, 0, data.length - 1);
    }

    /**
     * @param data
     * @param needle
     * @return positive number if needle is found
     *         negative number if needle is not found,
     *         should be inserted at position -x-1
     */
    static int search2(int[] data, int needle) {
        int l = 0;
        int r = data.length - 1;
        while (l <= r) {
            int m = (l + r) >> 1;
            if      (needle < data[m]) r = m - 1;
            else if (needle > data[m]) l = m + 1;
            else /* needle == data[m] */ return m;
        }
        return ~l;
//        return -(l + 1);
    }

    static <A extends Comparable<A>> int search4(A[] data, A needle) {
        int l = 0;
        int r = data.length - 1;
        while (l <= r) {
            int m = (l + r) >> 1;
            if      (needle.compareTo(data[m]) < 0) r = m - 1;
            else if (needle.compareTo(data[m]) > 0) l = m + 1;
            else /* needle == data[m] */ return m;
        }
        return ~l;
    }

    static <A> int search4(A[] data, A needle, Comparator<A> cmp) {
        int l = 0;
        int r = data.length - 1;
        while (l <= r) {
            int m = (l + r) >> 1;
            if      (cmp.compare(needle, data[m]) < 0) r = m - 1;
            else if (cmp.compare(needle, data[m]) > 0) l = m + 1;
            else /* needle == data[m] */ return m;
        }
        return ~l;
    }

    static SearchResult search3(int[] data, int needle) {
        int r = search2(data, needle);
        if (r >= 0) return new SearchResult.Found(r);
        return new SearchResult.NotFound(~r);
    }

    static <A extends Comparable<A>> SearchResult search3(A[] data, A needle) {
        int r = search4(data, needle);
        if (r >= 0) return new SearchResult.Found(r);
        return new SearchResult.NotFound(~r);
    }

    public static void main0(String[] args) {

        int[] data = new Random().ints(30, 10, 99).toArray();

        System.out.println(Arrays.toString(data));

        Set<Integer> data2 = Arrays.stream(data).boxed()
                .collect(Collectors.toSet());

        Set<Integer> data3 = Arrays.stream(data).boxed()
                .collect(Collectors.toCollection(
                        () -> new HashSet<>(data.length)
                ));

        Arrays.sort(data);
        System.out.println(Arrays.toString(data));

        System.out.println(search(data, 9));                                       // false
        System.out.println(search(data, data[(int) (Math.random()* data.length)]));// true
        System.out.println(search(new int[0], 1));

        System.out.println(search2(data, data[0]   )); // 0, found At 0
        System.out.println(search2(data, data[0]-10)); // -1, insert At 0

        SearchResult sr1 = search3(data, data[0]);
        SearchResult sr2 = search3(data, data[0] - 10);

        System.out.println(sr1); // Found at: 0
        System.out.println(sr2); // Not Found. Insertion index: 0

        int index = Arrays.binarySearch(data, 33);


    }

    public static boolean isAddSafe(int x, int y) {
        int r = x + y;
        // return ............... > 0;
        if (x > 0 && y > 0 && r < 0) return false;
        if (x < 0 && y < 0 && r > 0) return false;
        return true;
    }

    public static int safeAdd(int x, int y) {
        if (isAddSafe(x, y)) return x + y;
        throw new IllegalArgumentException(String.format("%d + %d will overflow", x, y));
    }

    public static void main(String[] args) {
        System.out.println(Integer.MAX_VALUE + Integer.MAX_VALUE);
        System.out.println(safeAdd(Integer.MAX_VALUE-1, 1));
        System.out.println(safeAdd(Integer.MIN_VALUE+1, -2)); // Integer.MIN_VALUE-1
        System.out.println(safeAdd(Integer.MAX_VALUE-1, 2)); // Integer.MAX_VALUE+1
//        System.out.println(isAddSafe(Integer.MAX_VALUE, 1));
    }

}
