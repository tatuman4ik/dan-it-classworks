package a6binarysearch;

public interface SearchResult {

    boolean found();
    int position();

    class Found implements SearchResult {

        private final int position;

        public Found(int position) {
            this.position = position;
        }

        @Override
        public boolean found() {
            return true;
        }

        @Override
        public int position() {
            return position;
        }

        @Override
        public String toString() {
            return String.format("Found at: %d", position);
        }
    }

    class NotFound implements SearchResult {

        private final int position;

        public NotFound(int position) {
            this.position = position;
        }

        @Override
        public boolean found() {
            return false;
        }

        @Override
        public int position() {
            return position;
        }

        @Override
        public String toString() {
            return String.format("Not Found. Insertion index: %d", position);
        }
    }


}
