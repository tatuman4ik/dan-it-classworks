package a8graph;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class XGraphNonDirected {
    private final int vertexCount;
    private final List<LinkedList<Integer>> edges;

    public XGraphNonDirected(int count) {
        this.vertexCount = count;
        this.edges = IntStream.range(0, vertexCount)
                .mapToObj(x -> new LinkedList<Integer>())
                .toList();
    }

    public void add(int vertex_src, int vertex_dst) {
        LinkedList<Integer> to = edges.get(vertex_src);
        if (to.contains(vertex_dst)) return;
        to.add(vertex_dst);

        LinkedList<Integer> to2 = edges.get(vertex_dst);
        if (to2.contains(vertex_src)) return;
        to2.add(vertex_src);
    }

    public void remove(int vertex_src, int vertex_dst) {
        edges.get(vertex_src).remove((Integer) vertex_dst);
    }

    public LinkedList<Integer> getEdgesFrom(int vertex_from) {
        return edges.get(vertex_from);
    }

}
