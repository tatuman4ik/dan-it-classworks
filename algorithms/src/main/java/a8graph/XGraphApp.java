package a8graph;

import java.util.Collection;
import java.util.Optional;

public class XGraphApp {

    public static void main(String[] args) {

        XGraphDirected g = new XGraphDirected(100);
        g.add(1, 2);
        g.add(1, 3);
        g.add(2, 4);
        g.add(2, 5);
        g.add(4, 8);
        g.add(4, 9);
        g.add(5, 10);
        g.add(5, 11);
        g.add(3, 6);
        g.add(3, 7);
        g.add(7, 14);
        g.add(6, 13);
        g.add(6, 12);
        g.add(12, 15);
        g.add(12, 16);
        g.add(13, 7);
        g.add(7, 3);
        g.add(13, 17);
        g.add(13, 18);

        GraphTasks t = new GraphTasks(g);
        System.out.println(t.isConnected(1, 18));
        System.out.println(t.isConnected(18, 1));

        Optional<Collection<Integer>> path = t.path(1, 18);
        System.out.println(path);

        Optional<Collection<Integer>> path2 = t.path(18, 1);
        System.out.println(path2);

        System.out.println(t.dfs(1));
        System.out.println(t.bfs(1));
    }


}
