package a8graph;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class XGraphDirected {
    public final int vertexCount;
    private final List<LinkedList<Integer>> edges;

    public XGraphDirected(int count) {
        this.vertexCount = count;
        this.edges = IntStream.range(0, vertexCount)
                .mapToObj(x -> new LinkedList<Integer>())
                .toList();
    }

    public void add(int vertex_src, int vertex_dst) {
        LinkedList<Integer> to = edges.get(vertex_src);
        if (to.contains(vertex_dst)) return;
        to.add(vertex_dst);
    }

    public void remove(int vertex_src, int vertex_dst) {
        edges.get(vertex_src).remove((Integer) vertex_dst);
    }

    public LinkedList<Integer> children(int vertex_from) {
        return edges.get(vertex_from);
    }

}
