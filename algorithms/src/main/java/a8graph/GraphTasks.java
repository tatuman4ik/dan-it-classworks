package a8graph;

import java.util.*;

public class GraphTasks {

    private final XGraphDirected g;

    public GraphTasks(XGraphDirected g) {
        this.g = g;
    }

    // > 1
    private boolean isConnected2(int src, int dst, int[] visited) {
        if (visited[src] > 0) { visited[src]++; return false; }
        visited[src]++;
        if (src == dst) return true;
        List<Integer> children = g.children(src);
        for (int child: children) {
            if (isConnected2(child, dst, visited)) return true;
        }
        return false;
    }

    private boolean isConnected(int src, int dst, boolean[] visited) {
        if (visited[src]) return false;
        visited[src] = true;
        if (src == dst) return true;
        List<Integer> children = g.children(src);
        for (int child: children) {
            if (isConnected(child, dst, visited)) return true;
        }
        return false;
    }

    public boolean isConnected(int src, int dst) {
        return isConnected(src, dst, new boolean[g.vertexCount]);
    }

    private Optional<Collection<Integer>> path(int src, int dst, boolean[] visited, Stack<Integer> trace) {
        if (visited[src]) return Optional.empty();
        visited[src] = true;
        trace.push(src);
        if (src == dst) return Optional.of(trace);
        List<Integer> children = g.children(src);
        for (int child: children) {
            Optional<Collection<Integer>> path = path(child, dst, visited, trace);
            if (path.isPresent()) return path;
        }
        trace.pop();
        return Optional.empty();
    }

    public Optional<Collection<Integer>> path(int vertex_src, int vertex_dst) {
        return path(vertex_src, vertex_dst, new boolean[g.vertexCount], new Stack<>());
    }

    private Collection<Integer> dfs(int curr, boolean[] visited) {
        if (visited[curr]) return Collections.emptyList();
        LinkedList<Integer> out = new LinkedList<>() {{ add(curr); }};
        visited[curr] = true;
        g.children(curr).forEach(child -> out.addAll(dfs(child, visited)));
        return out;
    }

    // Traverse all the Graph. Depth First
    public Collection<Integer> dfs(int src) {
        return dfs(src, new boolean[g.vertexCount]);
    }

    private void bfs(boolean[] visited, LinkedList<Integer> process, Collection<Integer> outcome) {
        while (!process.isEmpty()) {
            int curr = process.pop();
            if (!visited[curr]) {
                visited[curr] = true;
                outcome.add(curr);

                LinkedList<Integer> children = g.children(curr);
                process.addAll(children);
            }
        }
    }

    public Collection<Integer> bfs(int src) {
        LinkedList<Integer> process = new LinkedList<>();
        LinkedList<Integer> outcome = new LinkedList<>();
        process.add(src);
        bfs(new boolean[g.vertexCount], process, outcome);
        return outcome;
    }
}
