package a7tree;

public class RotationApp {

    public static void main(String[] args) {

        XTree<Integer, Integer> x = new XTree<>();
        x.put(5, null);
        x.put(3, null);
        x.put(1, null);
        x.put(4, null);
        x.put(6, null);
        System.out.println(x);
        System.out.println("-".repeat(50));

        x.root = x.rotateRight(x.root);

        System.out.println(x);


    }

}
