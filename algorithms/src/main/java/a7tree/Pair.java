package a7tree;

record Pair<K, V>(K key, V value) {}