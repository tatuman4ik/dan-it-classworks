package a7tree;

import java.util.stream.IntStream;

public class Rotation2App {

    public static void main(String[] args) {
        XTree<Integer, Integer> x = new XTree<>();
        IntStream.rangeClosed(1, 15).forEach(x::put);
        System.out.println(x);
        System.out.println(x.count());
    }

}
