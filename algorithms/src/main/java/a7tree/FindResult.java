package a7tree;

import java.util.Optional;


public interface FindResult<K, V> {

    boolean isFound();
    Optional<Pair<K, V>> smaller();
    Optional<Pair<K, V>> bigger();

    class Found<K, V> implements FindResult<K, V> {
        @Override
        public boolean isFound() {
            return true;
        }

        @Override
        public Optional<Pair<K, V>> smaller() {
            return Optional.empty();
        }

        @Override
        public Optional<Pair<K, V>> bigger() {
            return Optional.empty();
        }
    }

    class NotFound<K, V> implements FindResult<K, V> {

        private final Pair<K, V> smaller;
        private final Pair<K, V> bigger;

        public NotFound(Pair<K, V> smaller, Pair<K, V> bigger) {
            this.smaller = smaller;
            this.bigger = bigger;
        }

        @Override
        public boolean isFound() {
            return false;
        }

        @Override
        public Optional<Pair<K, V>> smaller() {
            return Optional.ofNullable(smaller);
        }

        @Override
        public Optional<Pair<K, V>> bigger() {
            return Optional.ofNullable(bigger);
        }

        @Override
        public String toString() {
            return "NotFound{" +
                    "smaller=" + smaller +
                    ", bigger=" + bigger +
                    '}';
        }
    }


}


