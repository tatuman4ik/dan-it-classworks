package a7tree;

import java.util.Optional;

import static tools.EX.NI;

public class XTree<K extends Comparable<K>, V> {

    class Node {
        K key;
        V value;
        Node left;
        Node right;
        int n = 1;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public int diff() {
            return nodeSize(this.left) - nodeSize(this.right);
        }

        public void updateSize() {
            this.n = nodeSize(this.left) + 1 + nodeSize(this.right);
        }
    }

    public Node root;

    Optional<Pair<K, V>> contains(K key) {
        Node node = root;
        while (node != null) {
            int cmp = key.compareTo(node.key);
            if      (cmp < 0) node = node.left;
            else if (cmp > 0) node = node.right;
            else return Optional.of(new Pair<>(key, node.value));
        }
        return Optional.empty();
    }

    private Optional<Pair<K, V>> containsR(K key, Node node) {
        if (node == null) return Optional.empty();
        int cmp = key.compareTo(node.key);
        if      (cmp < 0) return containsR(key, node.left);
        else if (cmp > 0) return containsR(key, node.right);
        else return Optional.of(new Pair<>(key, node.value));
    }

    Optional<Pair<K, V>> containsR(K key) {
        return containsR(key, root);
    }

    int nodeSize(Node x) {
        return x == null ? 0 : x.n;
    }

    Node put(Node x, K key, V value) {
        if (x == null) return new Node(key, value);
        int cmp = key.compareTo(x.key);
        if      (cmp < 0) x.left  = put(x.left, key, value);
        else if (cmp > 0) x.right = put(x.right, key, value);
        else              x.value = value;
        x.updateSize();
        return rebalance(x);
    }

    void put(K key, V value) {
        root = put(root, key, value);
    }

    void put(K key) {
        put(key, null);
    }

    private Node findMinWithLeftFree(Node x) {
        return x.left == null ? x : findMinWithLeftFree(x.left);
    }

    private Node deleteMinAndPullUp(Node x) {
        if (x.left == null) return x.right;
        x.left = deleteMinAndPullUp(x.left);
        return x;
    }

    private Node performRemoval(Node x) {
        Node leftSubTree = x.left;
        Node attachTo = findMinWithLeftFree(x.right);
        Node newRight = deleteMinAndPullUp(x.right);
        attachTo.right = newRight;
        attachTo.left = leftSubTree;
        return attachTo;
    }

    Node remove(Node x, K key) {
        if (x == null) return null;
        int cmp = key.compareTo(x.key);
        if      (cmp < 0) x.left  = remove(x.left, key);
        else if (cmp > 0) x.right = remove(x.right, key);
        else /* cmp == 0 */ {
            if (x.left == null) return x.right;
            if (x.right == null) return x.left;
            x = performRemoval(x);
        }
        return x;
    }

    void remove(K key) {
        root = remove(root, key);
    }

    FindResult<K, V> find(K key) {
        Node node = root;
        Node bigger = null;
        Node smaller = null;
        while (node != null) {
            int cmp = key.compareTo(node.key);
            if      (cmp < 0) {
                bigger = node;
                node = node.left;
            }
            else if (cmp > 0) {
                smaller = node;
                node = node.right;
            }
            else return new FindResult.Found<>();
        }
        return new FindResult.NotFound<>(
                smaller == null ? null : new Pair<>(smaller.key, smaller.value),
                bigger == null ? null : new Pair<>(bigger.key, bigger.value)
        );
    }

    int count(Node node) {
        return node == null ? 0 : 1 + count(node.left) + count(node.right);
    }

    int count() {
        return count(root);
    }

    Optional<K> min(Node node) {
        if (node == null) return Optional.empty();
        if (node.left == null) return Optional.of(node.key);
        return min(node.left);
    }

    Optional<K> min() {
        return min(root);
    }

    public Node rotateRight(Node node) {
        Node n = node.left;
        node.left = n.right;
        n.right = node;

        node.updateSize();
        n.updateSize();

        return n;
    }

    private Node rotateLeft(Node node) {
        Node n = node.right;
        node.right = n.left;
        n.left = node;

        node.updateSize();
        n.updateSize();

        return n;
    }

    private Node rebalance(Node node) {
        int diff = node.diff();
        if (Math.abs(diff) < 2) return node;
        return diff > 0 ? rotateRight(node) : rotateLeft(node);
    }

    // TODO: homework
    int width() {
        throw NI;
    }

    // TODO: homework
    int height() {
        throw NI;
    }

    private int depth(Node node) {
        return node == null ? 0 : 1 + Math.max(depth(node.left), depth(node.right));
    }

    public int depth() {
        return depth(root);
    }

    private String padded(K key) {
        //        '  1 '
        //        ' 10 '
        //        '100 '
        return String.format("%3s ", key.toString());
    }

    @Override
    public String toString() {
        int depth = depth();
        int width = 1 << depth;  // 2^depth

        // TODO: homework
        return super.toString();
    }



}
