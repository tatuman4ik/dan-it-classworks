package a7tree;

public class App {

    public static void main(String[] args) {
        XTree<Integer, String> t = new XTree<>();

        t.put(10, "Jim");
        t.put(20, "Jack");
        t.put(30, "Jeremy");
        t.put(25, "Bill");
        t.put(32, "Sergio");
        t.put(15, "Alex");
        t.put(5, "Alexandro");

        System.out.println("=".repeat(50));
        System.out.println(t);
        System.out.println("=".repeat(50));

        System.out.println(t.contains(32)); // Optional[Pair[key=32, value=Sergio]]
        System.out.println(t.find(31)); // NotFound{smaller=Pair[key=30, value=Jeremy], bigger=Pair[key=32, value=Sergio]}

        System.out.println(t.find(20));
        t.remove(20);
        System.out.println(t.find(20));
        System.out.println(t);
        System.out.println("=".repeat(50));
        t.put(35, null);
        t.put(45, null);
        t.put(55, null);
        System.out.println(t);
        System.out.println("=".repeat(50));
    }


}
