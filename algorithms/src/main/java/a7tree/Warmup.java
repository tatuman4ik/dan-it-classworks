package a7tree;

import java.util.stream.Stream;

public class Warmup {

    /**
     * 0 - List[]
     * 1 - List[0, 1]
     * 2 - List[00, 01, 10, 11]
     * 3 - List[000, 001, 010, 011, 100, 101, 110, 111]
     * 4 - List[0000, 0001, 0010, 0011, 0100, 0101, 0110, 0111, 1000, 1001, 1010, 1011, 1100, 1101, 1110, 1111]
     */
    static Stream<String> combinations(int n) {
        if (n == 0) return Stream.of("");
        return Stream.of("0", "1")
                .flatMap(d ->
                        combinations(n - 1).map(s -> d + s)
                );
    }

    public static void main(String[] args) {
        Stream.of(1, 2).flatMap(x -> Stream.of(-x, x)).forEach(x -> System.out.printf("%d ", x));
        System.out.println();

        System.out.println(combinations(0).toList());
        System.out.println(combinations(1).toList());
        System.out.println(combinations(2).toList());
        System.out.println(combinations(3).toList());
        System.out.println(combinations(4).toList());
    }
}
