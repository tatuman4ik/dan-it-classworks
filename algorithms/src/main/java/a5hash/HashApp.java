package a5hash;

import java.util.HashMap;

public class HashApp {

    public static void main0(String[] args) {
        HashMap<Integer, String> m = new HashMap<>(1024, 0.5f);
        m.put(1024, "Jim");
        m.put(153, "Alex");

        String s1 = m.get(1024); // O(1)
        String s2 = m.get(153);  // O(1)
    }


    public static void main(String[] args) {
        XMap m = new XMap();

        m.putV2(135, "Jim");
        m.putV2(637, "Jack");
        m.putV2(932, "Alex");

        System.out.println(m.getV1(135)); // Jim
        System.out.println(m.getV1(637)); // Jack
        System.out.println(m.getV1(932)); // Alex
        System.out.println(m.getV1(111));
    }

}
