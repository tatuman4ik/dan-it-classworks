package a5hash;

public class XMap {

    private final double k_fill;
    private final double k_grow;

    Object[] keys;
    Object[] data;
    private int count = 0;

    public XMap(int size, double k_fill, double k_grow) {
        this.k_fill = k_fill;
        this.k_grow = k_grow;
        this.keys = new Object[size];
        this.data = new Object[size];
    }

    public XMap() {
        this(16, 0.7, 1.5);
    }

    private int f(int key) {
        return key % data.length;
    }

    private void grow() {
        String[] a2 = new String[(int) (data.length * k_grow)];
        Integer[] k2 = null;
        // smart copy KEYS + VALUES according old and new indexes
        System.arraycopy(keys, 0, k2, 0, keys.length);
        System.arraycopy(data, 0, a2, 0, data.length);
        data = a2;
        keys = k2;
    }

    private boolean occupied(int index) {
        return keys[index] != null;
    }

    public void putV1(int key, String value) {
        // to allow more-less optimal access time
        if ((double)count / data.length > k_fill) grow();
        int index = f(key);
        while (occupied(index)) index++;
        data[index] = value;
        count++;
    }

    public void putV2(int key, String value) {
        if ((double)count / data.length > k_fill) grow();
        count++;
        int index = f(key);
        Object atIndex = keys[index];

        // if cell is empty
        if (atIndex == null) {
            keys[index] = Integer.valueOf(key);
            data[index] = value;
            return;
        }

        // cell is already occupied, but no duplicates
        if (atIndex instanceof Integer) {
            // convert `Integer` to `Integer[]`
            Integer[] keysAt = new Integer[10];
            keysAt[0] = (Integer) atIndex;
            keys[index] = keysAt;

            String[] valuesAt = new String[10];
            valuesAt[0] = (String) data[index];
            data[index] = valuesAt;
            return;
        }

        // cell is already occupied, and HAS duplicates
        // cell is Integer[]
        Integer indexVert = null; // TODO: + implement grow
        ((Integer[])keys[index])[indexVert] = key;
        ((String[])data[index])[indexVert] = value;
    }

    public String getV1(int key) {
        int index = f(key);
        while ((Integer) keys[index] != key) index++;
        return (String) data[index];
    }

}
