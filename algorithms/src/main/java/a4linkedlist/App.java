package a4linkedlist;

public class App {

    public static void main(String[] args) {
        XLinkedList ll = new XLinkedList();
        System.out.println(ll.length());
        System.out.println(ll.containsR(33));
        ll.prepend(10);
        ll.appendR2(20);
        ll.appendR2(30);
        ll.appendR2(40);
        ll.prepend(5);
        ll.prepend(1);
        System.out.println(ll); // 1 -> 5 -> 10 -> 20 -> 30
        System.out.println(ll.containsR(5));
        System.out.println(ll.length());
        System.out.println(ll.lengthR());
        System.out.println(ll.lengthTR());
        ll.insertAfter(100, x -> x == 5);
        ll.insertBefore(200, 1);
        System.out.println(ll); // {[200] -> [1] -> [5] -> [100] -> [10] -> [20] -> [30] -> [40]}
        ll.reverse();
        System.out.println(ll);
        ll.reverseR();
        System.out.println(ll);

    }

}
