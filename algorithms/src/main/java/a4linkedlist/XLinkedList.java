package a4linkedlist;

import java.util.Optional;
import java.util.function.Predicate;

public class XLinkedList {

    class Node {
        int value;
        Node next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }

        public Node(int value) {
            this(value, null);
        }

        @Override
        public String toString() {
            return String.format("[%d]", value);
        }
    }

    private Node head;

    public boolean contains(int x) {
        Node curr = head;
        while (curr != null) {
            if (curr.value == x) return true;
            curr = curr.next;
        }
        return false;
    }

    // recursive == tail recursive
    private boolean containsR(int x, Node curr) {
        if (curr == null) return false;
        if (curr.value == x) return true;
        return containsR(x, curr.next);
    }

    public boolean containsR(int x) {
        return containsR(x, head);
    }

    public String toStringIt() {
        StringBuilder sb = new StringBuilder("{");
        Node curr = head;
        while (curr != null) {
            if (curr != head) sb.append(" -> ");
            sb.append(curr);
            curr = curr.next;
        }
        return sb.append("}").toString();
    }

    private String toStringR(Node curr, StringBuilder sb) {
        if (curr == null) return sb.append("}").toString();
        if (curr != head) sb.append(" -> ");
        sb.append(curr);
        return toStringR(curr.next, sb);
    }

    public String toStringR() {
        return toStringR(head, new StringBuilder("{"));
    }

    @Override
    public String toString() {
//        return toStringIt();
        return toStringR();
    }

    // add to head
    public void prepend(int x) {
//        Node node = new Node(x);
//        node.next = head;
//        head = node;
        head = new Node(x, head);
    }

    public void append(int x) {
        if (head == null) {
            prepend(x);
            return;
        }

        Node curr = head;
        while (curr.next != null) {
            curr = curr.next;
        }
        curr.next = new Node(x);
    }

    private void appendR1(int x, Node curr) {
        if (curr.next == null) {
            curr.next = new Node(x);
            return;
        }
        appendR1(x, curr.next);
    }

    public void appendR1(int x) {
        if (head == null) {
            prepend(x);
            return;
        }
        appendR1(x, head);
    }

    private Node appendR2(int x, Node curr) {
        if (curr == null) return new Node(x);
        curr.next = appendR2(x, curr.next);
        return curr;
    }

    public void appendR2(int x) {
        head = appendR2(x, head);
    }

    public int length() {
        int l = 0;
        Node curr = head;
        while (curr != null) {
            l++;
            curr = curr.next;
        }
        return l;
    }

    // length recursive - private, implementation
    private int lengthR(Node curr) {
        if (curr == null) return 0;
        return 1 + lengthR(curr.next);
    }

    // length recursive - public, runner part
    public int lengthR() {
        return lengthR(head);
    }

    // length tail recursive - private, implementation
    private int lengthTR(Node curr, int acc) {
        if (curr == null) return acc;
        return lengthTR(curr.next, acc + 1);
    }

    // length tail recursive - public, runner part
    public int lengthTR() {
        return lengthTR(head, 0);
    }

    public void reverse() {
        Node curr = head;
        Node prev = null;
        while (curr != null) {
            Node savedNext = curr.next;
            curr.next = prev;
            prev = curr;
            curr = savedNext;
        }
        head = prev;
    }

    private Node reverseR(Node curr, Node prev) {
        if (curr == null) return prev;
        Node savedNext = curr.next;
        curr.next = prev;
        return reverseR(savedNext, curr);
    }

    public void reverseR() {
        head = reverseR(head, null);
    }

    // TODO: homework - remove
    public void remove(Predicate<Integer> func) {}
    // TODO: homework - remove
    public void removeAfter(Predicate<Integer> func) {}
    // TODO: homework - remove
    public void removeBefore(Predicate<Integer> func) {}

//  public void insertAfter(int x, Predicate<Integer> func)
//  public boolean insertAfter(int x, Predicate<Integer> func)
//  public void    insertAfter(int x, Predicate<Integer> func) throws Exception
//  public void    insertAfterOrTail(int x, Predicate<Integer> func)

    private Optional<Node> findNode(Predicate<Integer> func) {
        Node curr = head;
        while (curr != null) {
            if (func.test(curr.value)) return Optional.of(curr);
            curr = curr.next;
        }
        return Optional.empty();
    }

    private Optional<Node> findNextNode(Predicate<Integer> func) {
        Node curr = head;
        while (curr != null && curr.next != null) {
            if (func.test(curr.next.value)) return Optional.of(curr);
            curr = curr.next;
        }
        return Optional.empty();
    }

    // insert                      f: Int => Boolean
    public boolean insertAfter(int x, Predicate<Integer> func) {
        Optional<Node> found = findNode(func);
        if (found.isEmpty()) return false;
        found.ifPresent(node -> node.next = new Node(x, node.next));
        return true;
    }

    public boolean insertAfter(int x, int n) {
        return insertAfter(x, a -> a == n);
    }

    public boolean insertBefore(int x, Predicate<Integer> func) {
        if (head != null && func.test(head.value)) {
            prepend(x);
            return true;
        }
        Optional<Node> found = findNextNode(func);
        if (found.isEmpty()) return false;
        found.ifPresent(node -> node.next = new Node(x, node.next));
        return true;
    }

    public boolean insertBefore(int x, int n) {
        return insertBefore(x, a -> a == n);
    }


}
