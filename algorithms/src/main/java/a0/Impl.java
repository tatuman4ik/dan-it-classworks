package a0;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static tools.EX.NI;

public class Impl {

  public String f1(int x) {
    return Integer.toString(x);
  }

  public int f2(String s) {
    return s.length();
  }

  public String f3(Optional<String> x) {
    return "";
//    if (x.isEmpty()) {
//
//    }
  }

  <A, B> B f5(A a, Function<A, B> f) {
    return f.apply(a);
  }

  // A => B
  // B => C
  // A => C
  <A, B, C> C f5(Function<A, B> f1, Function<B, C> f2) {
    throw NI;
  }

  <A, B, C> Function<A, C> f6(Function<A, B> f1, Function<B, C> f2) {
    return a -> {
      B b = f1.apply(a);
      C c = f2.apply(b);
      return c;
    };
  }

  <A> A f4(Optional<A> x) {
    throw NI;
  }

  <A> List<A> f7(List<Optional<A>> xs) {
    return xs.stream().filter(x -> x.isPresent()).map(x -> x.get()).toList();
  }

  <A> List<A> f8() {
    return new ArrayList<>();
  }

}
