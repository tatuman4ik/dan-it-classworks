package a9lee;

import a9lee.colored.Ansi;
import a9lee.colored.Attribute;
import a9lee.colored.Colored;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Lee {

    private final int EMPTY = 0;
    private final int START = 1;
    private final int OBSTACLE = -9;
    private final int width;
    private final int height;
    private final int[][] board;

    public Lee(int width, int height) {
        this.width = width;
        this.height = height;
        this.board = new int[height][width];
    }

    private int get(int x, int y) {
        return board[y][x];
    }

    private void set(int x, int y, int value) {
        board[y][x] = value;
    }

    private int get(Point point) {
        return get(point.x(), point.y());
    }

    private void set(Point point, int value) {
        set(point.x(), point.y(), value);
    }

    private boolean isOnBoard(Point point) {
        return point.x() >= 0 && point.x() < width &&
                point.y() >= 0 && point.y() < height;
    }

    private boolean isUnvisited(Point point) {
        return get(point) == EMPTY;
    }

    private Supplier<Stream<Point>> deltas() {
        return () -> Stream.of(
                Point.of(-1, 0), // offset, not a point
                Point.of(1, 0),  // offset, not a point
                Point.of(0, 1),  // offset, not a point
                Point.of(0, -1)  // offset, not a point
        );
    }

    private Stream<Point> neighbours(Point point) {
        return deltas().get()
                .map(d -> point.move(d.x(), d.y()))
                .filter(this::isOnBoard);
    }

    private Stream<Point> neighboursUnvisited(Point point) {
        return neighbours(point)
                .filter(this::isUnvisited);
    }

    private List<Point> neighboursByValue(Point point, int value) {
        return neighbours(point)
                .filter(p -> get(p) == value)
                .toList();
    }

    private void initializeBoard(List<Point> obstacles) {
        obstacles.forEach(p -> set(p, OBSTACLE));
    }

    public Optional<List<Point>> trace(Point start, Point finish, List<Point> obstacles) {
        // 1. initialization
        initializeBoard(obstacles);
        int[] counter = {START}; // HEAP due to lambda
        set(start, counter[0]);
        counter[0]++;
        boolean found = false;
        // 2. fill the board
        for (Set<Point> curr = new HashSet<>(Set.of(start)); !(found || curr.isEmpty()); counter[0]++) {
            Set<Point> next = curr.stream()
                    .flatMap(this::neighboursUnvisited)
                    .collect(Collectors.toSet());

            next.forEach(p -> set(p, counter[0]));
            found = next.contains(finish);
            curr = next;
        }
        // 3. backtrace (reconstruct the path)
        if (!found) return Optional.empty();
        LinkedList<Point> path = new LinkedList<>();
        path.add(finish);
        counter[0]--;

        Point curr = finish;
        while (counter[0] > START) {
            counter[0]--;
            Point prev = neighboursByValue(curr, counter[0]).get(0);
            if (prev == start) break;
            path.addFirst(prev);
            curr = prev;
        }
        return Optional.of(path);
    }

    public String cellFormatted(Point p, List<Point> path) {
        int value = get(p);
        String valueF = String.format("%3d", value);

        if (value == OBSTACLE) {
            Attribute a = new Attribute(Ansi.ColorFont.BLUE);
            return Colored.build(" XX", a);
        }

        if (path.isEmpty()) return valueF;

        if (path.contains(p)) {
            Attribute a = new Attribute(Ansi.ColorFont.RED);
            return Colored.build(valueF, a);
        }
        return valueF;//" --";
    }

    public String boardFormatted(List<Point> path) {
        return IntStream.range(0, height).mapToObj(y ->
                        IntStream.range(0, width)
                                .mapToObj(x -> Point.of(x, y))
                                .map(p -> cellFormatted(p, path))
                                .collect(Collectors.joining())
                )
                .collect(Collectors.joining("\n"));
    }

    @Override
    public String toString() {
        return boardFormatted(Collections.emptyList());
    }
}
