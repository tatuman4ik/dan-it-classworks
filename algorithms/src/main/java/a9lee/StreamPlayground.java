package a9lee;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamPlayground {

    public static void main(String[] args) {
        Stream<Integer> ints = Stream.of(1, 2, 3);
        ints.forEach(System.out::println);
        // IllegalStateException: stream has already been operated upon or closed
//        ints.forEach(System.out::println);

        System.out.println("=".repeat(50));

        Supplier<Stream<Integer>> ints2 = () -> Stream.of(1, 2, 3);
        ints2.get().forEach(System.out::println);
        ints2.get().forEach(System.out::println);
    }

}
