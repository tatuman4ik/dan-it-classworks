package a9lee;

public record Point(int x, int y) {

    public static Point of(int x, int y) {
        return new Point(x, y);
    }

    @Override
    public String toString() {
        return String.format("[%2d, %2d]", x, y);
    }

    public Point move(int dx, int dy) {
        return new Point(x + dx, y + dy);
    }

}
