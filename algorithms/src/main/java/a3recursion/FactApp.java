package a3recursion;

public class FactApp {

    int fact0(int n) {
        int result = 1;

        for (int i = 2; i <= n; i++) {
            result = result * i;
        }

        return result;
    }

    int fact(int n) {
        if (n == 0) return 1;
        return n * fact(n - 1);
    }

    int fact2(int n, int a) {
        if (n == 0) return a;
        return fact2(n - 1, a * n);
    }

    int fact2(int n) {
        return fact2(n, 1);
    }

    public static void main(String[] args) {
        FactApp f = new FactApp();

        System.out.println(f.fact0(5));
        System.out.println(f.fact(5));
        System.out.println(f.fact2(5));
        // 1000000 wanted
        //  994687 failed at
        //    5313 could be performed
    }


}
