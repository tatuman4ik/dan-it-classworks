package a3recursion;

import java.util.HashMap;
import java.util.Map;

public class FiboApp {

    int fibo(int n) {
        if (n == 0 || n == 1) return 1;
        return fibo(n - 1) + fibo(n - 2);
    }

    long fibo2(int nth) {
        long n1 = 1;
        long n2 = 1;
        long n = 0;

        for (int i = 2; i <= nth; i++) {
            n = n1 + n2;
            n1 = n2;
            n2 = n;
        }

        return n;
    }

    private long fibo2tr(long n1, long n2, long c) {
        if (c == 0) return n2;
        return fibo2tr(n2, n1 + n2, c - 1);
    }

    long fibo2tr(long n) {
        return fibo2tr(1, 1, n-1);
    }

    private long fiboCached(long n, Map<Long, Long> cache) {
        if (n == 0 || n == 1) return 1L;

        if (cache.containsKey(n)) return cache.get(n);

        long nth = fiboCached(n - 1, cache) + fiboCached(n - 2, cache);
        cache.put(n, nth);

        return nth;
    }

    long fiboCached(long n) {
        return fiboCached(n, new HashMap<>());
    }

    public static void main(String[] args) {
        FiboApp f = new FiboApp();
//        System.out.println(f.fibo(100));
        System.out.println(f.fiboCached(100));
        System.out.println(f.fibo2tr(100));
//        System.out.println(f.fibo2(10));
    }


}
