package a1sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

import static tools.EX.NI;

public class SortApp {

    static int[] sort1(int[] data) {
        throw NI;
    }

    static void sort2(int[] data) {
        for (int i = 0; i < data.length; i++) {
            for (int j = i+1; j < data.length; j++) {
                if (data[i] > data[j]) {
                    int t = data[i];
                    data[i] = data[j];
                    data[j] = t;
                }
            }
        }
    }

    static <A extends Comparable<A>> void sort3(A[] data) {
        for (int i = 0; i < data.length; i++) {
            for (int j = i+1; j < data.length; j++) {
                if (data[i].compareTo(data[j]) > 0) {
                    A t = data[i];
                    data[i] = data[j];
                    data[j] = t;
                }
            }
        }
    }

    static <A> void swap(A[] data, int i, int j) {
        A t = data[i];
        data[i] = data[j];
        data[j] = t;
    }

    static <A> void sort3(A[] data, Comparator<A> cmp) {
        for (int i = 0; i < data.length; i++) {
            for (int j = i+1; j < data.length; j++) {
                if (cmp.compare(data[i], data[j]) > 0) {
                    swap(data, i, j);
                }
            }
        }
    }

    static int[] merge0(int[] xs, int[] ys) {
        int[] zs = new int[xs.length + ys.length];
        int i = 0; // xs
        int j = 0; // ys
        int k = 0; // zs
        while (i < xs.length && j < ys.length) {
            int xsi = xs[i];
            int ysj = ys[j];
            if (xsi < ysj) {
                zs[k] = xsi;
                i++;
            } else {
                zs[k] = ysj;
                j++;
            }
            k++;
        }

        if (i < xs.length)
            System.arraycopy(xs, i, zs, k, zs.length - k);
        if (j < ys.length)
            System.arraycopy(ys, j, zs, k, zs.length - k);
        return zs;
    }

    static int[] merge(int[] xs, int[] ys) {
        int[] zs = new int[xs.length + ys.length];
        int i = 0; // xs
        int j = 0; // ys
        int k = 0; // zs
        while (i < xs.length && j < ys.length)
            zs[k++] = xs[i] < ys[j] ? xs[i++] : ys[j++];
        while (i < xs.length)
            zs[k++] = xs[i++];
        while (j < ys.length)
            zs[k++] = ys[j++];
        return zs;
    }

    public static void main2(String[] args) {
        int[] data = new Random().ints(30, 10, 90)
                .toArray();
        System.out.println(Arrays.toString(data));
        sort2(data);
        System.out.println(Arrays.toString(data));
    }

    public static void main(String[] args) {
        int[] xs = new Random().ints(10, 10, 90).toArray();
        int[] ys = new Random().ints(10, 10, 90).toArray();
        sort2(xs);
        sort2(ys);
        int[] zs = merge(xs, ys);
        System.out.println(Arrays.toString(xs));
        System.out.println(Arrays.toString(ys));
        System.out.println(Arrays.toString(zs));
    }

}
