package app.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "person")
public class Person {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "p_id")
  private Integer id;

  private String name;

  @OneToOne
  @JoinTable(
      name = "pe_ex",
      joinColumns = {
          @JoinColumn(name = "person_id", referencedColumnName = "p_id")
      },
      inverseJoinColumns = {
          @JoinColumn(name = "extra_id", referencedColumnName = "x_id")
      }
  )
  private Extra extra;

}
