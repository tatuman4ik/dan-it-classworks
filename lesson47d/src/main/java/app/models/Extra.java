package app.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Extra {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "x_id")
  private Long id;

  private String extraDetails;

  @OneToOne(mappedBy = "extra")
  private Person person;
}
