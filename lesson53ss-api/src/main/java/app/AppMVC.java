package app;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppMVC implements WebMvcConfigurer {

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    String[][] mappings = {
        //   HTTP      template file
        //    0           1
        {"/",      "menu" },
        {"/guest", "guest"},
        {"/home",  "home" },
        {"/me",    "me"   },
        {"/news",  "news" },
        {"/admin", "admin"},
    };

    for (String[] item: mappings) {
      registry.addViewController(item[0]).setViewName(item[1]);
    }

//    registry.addViewController("/app2/page1").setViewName("page1");
//    registry.addViewController("/app2/page2").setViewName("page2");
  }
}
