package app;

import app.security.JwtUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookController {

  @GetMapping("books")
  public List<String> allBooks(Authentication a) {
    JwtUserDetails principal = (JwtUserDetails) a.getPrincipal();
    return List.of(
        "Java",
        "Scala",
        principal.getId().toString()
    );
  }
}
