package app.auth;

import lombok.Data;

@Data
public class LoginRq {
  private String username;
  private String password;
  private Boolean rememberMe;
}
