package app.auth;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LoginRs {
  private Boolean status;
  private String error;
  private String token;

  static LoginRs Ok(String token) {
    return new LoginRs(true, null, token);
  }

  static LoginRs Error(String message) {
    return new LoginRs(false, message, null);
  }

}
