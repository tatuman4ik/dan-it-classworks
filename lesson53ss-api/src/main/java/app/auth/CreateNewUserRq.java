package app.auth;

import lombok.Data;

@Data
public class CreateNewUserRq {
  private String username;
  private String password;
}
