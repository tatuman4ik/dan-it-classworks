package app.auth;

import app.security.DbUser;
import app.security.DbUserRepo;
import app.security.JwtTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthController {

  private final DbUserRepo dbUserRepo;
  private final PasswordEncoder enc;
  private final JwtTokenService tokenService;

  @PostMapping("/reg")
  public ResponseEntity<?> handleCreate(@RequestBody CreateNewUserRq rq) {
    dbUserRepo.save(new DbUser(rq.getUsername(), enc.encode(rq.getPassword())));
    return ResponseEntity.ok().build();
  }

  @PostMapping("/login")
  public ResponseEntity<LoginRs> handleLogin(@RequestBody LoginRq rq) {
    return dbUserRepo.findByUsername(rq.getUsername())
        .filter(u -> enc.matches(rq.getPassword(), u.getPassword()))
        .map(u -> tokenService.generateToken(u.getId()))
        .map(LoginRs::Ok)
        .map(ResponseEntity::ok)
        .orElse(
            ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(LoginRs.Error("wrong user/password combination"))
            );
  }

}
