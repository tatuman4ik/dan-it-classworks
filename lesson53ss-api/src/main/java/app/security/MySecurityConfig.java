package app.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.List;

@Log4j2
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

  private final PasswordEncoder enc;
  private final DbUserRepo repo;
  private final JwtFilter jwtFilter;

  private void createOnce(){
    repo.saveAll(List.of(
            new DbUser("jim", enc.encode("123"), "ADMIN"),
            new DbUser("john", enc.encode("234"), "USER"),
            new DbUser("jack", enc.encode("345"), "ADMIN", "USER"),
            new DbUser("jeremy", enc.encode("456"))
        )
    );
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http.csrf().disable();

    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    http
        .authorizeRequests()
        .antMatchers("/resources/**").permitAll()
        .antMatchers("/auth/**").permitAll()
        .antMatchers("/guest/**").permitAll()
        .antMatchers("/home/**").authenticated()
        .antMatchers("/books/**").authenticated()
        .antMatchers("/admin/**").hasRole("ADMIN")
        .antMatchers("/me/**").hasRole("USER")
        .antMatchers("/news/**").hasAnyRole("ADMIN", "USER")
        .anyRequest().authenticated();

    http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);


    http
        .formLogin().permitAll();
  }

}
