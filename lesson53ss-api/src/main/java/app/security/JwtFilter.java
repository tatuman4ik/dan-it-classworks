package app.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Log4j2
@Component
@RequiredArgsConstructor
public class JwtFilter extends OncePerRequestFilter {

  private static final String BEARER = "Bearer ";
  private static final String AUTH_HEADER = HttpHeaders.AUTHORIZATION;

  private final JwtTokenService tokenService;

  private Optional<String> extractTokenFromRequest(HttpServletRequest rq) {
    return Optional.ofNullable(rq.getHeader(AUTH_HEADER))
        .filter(h -> h.startsWith(BEARER))
        .map(h -> h.substring(BEARER.length()));
  }

  @Override
  protected void doFilterInternal(HttpServletRequest rq, HttpServletResponse rs, FilterChain chain) throws ServletException, IOException {
    try {
      extractTokenFromRequest(rq)
          .flatMap(tokenService::parseToken)
          .map(JwtUserDetails::new)
          .map(ud -> new UsernamePasswordAuthenticationToken(ud, null, ud.getAuthorities()))
          .ifPresentOrElse(at -> {
            at.setDetails(new WebAuthenticationDetailsSource().buildDetails(rq));
            SecurityContextHolder.getContext().setAuthentication(at);
          }, () -> rs.setStatus(403));
      chain.doFilter(rq, rs);
    } catch (Exception x) {
      log.error("something went wrong in filter", x);
    }

  }

}
