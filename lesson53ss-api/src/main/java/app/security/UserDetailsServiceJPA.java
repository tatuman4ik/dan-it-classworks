package app.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Log4j2
@Configuration
@RequiredArgsConstructor
public class UserDetailsServiceJPA implements UserDetailsService {

  private final DbUserRepo repo;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    log.info("-------------- loading user: {}", username);
    return repo.findByUsername(username)
        .map(this::mapper)
        .orElseThrow(() -> new UsernameNotFoundException(
            String.format("user %s not found", username)
        ));
  }

  private UserDetails mapper(DbUser entry) {
    return User
        .withUsername(entry.getUsername())
        .password(entry.getPassword())
        // password already encoded in the db (during insertion)
        .roles(entry.getRoles())
        .build();
  }

}
