package playground;

import io.jsonwebtoken.*;
import lombok.extern.log4j.Log4j2;

import java.util.Date;
import java.util.Optional;

@Log4j2
public class Tokens {

  private static Long sec = 1            * 1000L;
  private static Long day = 60 * 60 * 24 * sec;
  private static Long week = day * 7;
  //  private String secret = "my-super-secret"; // bad
  private String secret = "bXktc3VwZXItc2VjcmV0"; // good


  public String generateToken(Integer userId, boolean rememberMe) {
    Date now = new Date();
    Date expiry = new Date(now.getTime() + (rememberMe ? week : day));
    return Jwts.builder()
        .setSubject(userId.toString()) // ANY string / JSON / whatever
        .setIssuedAt(now)
        .setExpiration(expiry)
        .signWith(SignatureAlgorithm.HS512, secret)
        .compact();
  }

  public Optional<Jws<Claims>> parseTokenToClaims(String token) {
    try {
      return Optional.of(Jwts.parser()
          .setSigningKey(secret)
          .parseClaimsJws(token));
    } catch (JwtException x) {
      log.error("something went wrong with token", x);
      return Optional.empty();
    }
  }

  public Optional<Integer> parseToken(String token) {
    return parseTokenToClaims(token)
//        .map(x -> {
//          System.out.println(x);
//          return x;
//        })
        .map(Jwt::getBody)
        .map(Claims::getSubject)
        .map(Integer::parseInt);
  }


  public static void main(String[] args) throws InterruptedException {
    Tokens t = new Tokens();

    String token = t.generateToken(123, false);
    System.out.println(token);
    Thread.sleep(2000);
    // http://localhost:8080/books?token=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjMiLCJpYXQiOjE2OTEwODIzMTAsImV4cCI6MTY5MTA5MDk1MH0.3kG7IA7-oG6kcF0RhSe_ZBYURSXerQklapV3n6e_VmqWQ_i-szJXOl947n_x-nq3zHJk2wWjoZyqcmvpWxvEUQ
    t.parseToken(token).ifPresent(System.out::println);
  }

}
